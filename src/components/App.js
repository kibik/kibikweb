import React, { Component, Fragment } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import AppNavbar from './AppNavbar';
import Login from "./auth/Login";
import Browse from "./game/Browse"
import Home from './Home';
import TournamentDetails from './game/TournamentDetails';
import WatchPlayedGames from './game/WatchPlayedGames';
import GameInfo from './game/GameInfo';
import NotFound from './errors/NotFound';
import Manage from './manage/Manage';
import history from '../history';

class App extends Component {
  render() {
    return (
      <Router history={history}>
        <Fragment>
          <AppNavbar></AppNavbar>
            <Switch>
              <Route path="/" exact component={ Home }></Route>
              <Route path="/login" exact render={() => <Login/> }></Route>
              <Route path="/browse" exact component={ Browse }></Route>
              <Route path='/browse/tournament/:tournamentId' exact component={ TournamentDetails } />
              <Route path='/browse/tournament/:tournamentId/watch' exact component={ WatchPlayedGames } />
              <Route path='/browse/tournament/watch/:gameId' exact component={ GameInfo } />
              <Route path='/manage' render={() => <Manage/> } />
              <Route component={ NotFound } /> 
            </Switch>
        </Fragment>
      </Router>
    )
  }
}

export default App;