import React from 'react';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import styled from 'styled-components';
import tournamentDefault from '../../images/tournament-default.jpg';

const CardWrapper = styled(Card)`
    margin: 10px;
    padding: 1px;
    border-radius: 12px;
    background-color: #ffe6ff
`;

class TournamentCard extends React.Component {
    renderDescription = (description) => {
        if(description && description.length > 50){
            return description.substring(0, 50) + "...";
        }

        return description;
    }

    renderPicture = () => {
        if(this.props.tournament.Logo){
            return this.props.tournament.Logo;
        }

        return tournamentDefault;
    }

    render(){ 
        let { Id, Name, Description } = this.props.tournament;

        let cardUrl = this.props.match ? this.props.match : '';
     
        return (
            <CardWrapper className="col-md-3">
                <Card.Img variant="top" src={ this.renderPicture() } />
                <Card.Body>
                    <Card.Title>{Name}</Card.Title>
                    <Card.Text>{this.renderDescription(Description)}</Card.Text>
                    <Link className="btn btn-primary btn-block" to={ cardUrl + `/browse/tournament/${Id}` }>Review</Link>
                </Card.Body>
            </CardWrapper>
        );
    }
}

const mapStateToProps = (state, ownProps) => {   
    let tournament = state.tournaments.tournaments.find(t => t.Id === ownProps.tournamentId);
  
    return { 
        tournament: {...tournament}
    };
}

export default connect(mapStateToProps, null)(TournamentCard);