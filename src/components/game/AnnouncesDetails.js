import React from 'react';
import styled from 'styled-components';
import { connect } from "react-redux";

const TableWrapper = styled.div`
    margin-right: 10px;
    max-height: 100px;
    overflow-y: auto;
    overflow-x: hidden;
`
const AnnounceTable = styled.table`
    width: 100%; 
    border-collapse: collapse;  
    background-color: #00ff99;
    border: 1px solid black;
`

const TableHead = styled.thead`
    font-weight: bold; 
    font-size: 0.65em;
`

const TableBody = styled.tbody`
    font-size: 0.575em;
`

const ThCell = styled.th`
    background-color: ${props => props.isInZone ? 'red' : 'white'}; 
    color: ${props => props.isInZone ? 'white' : 'black'} 
    padding: 0;
    border: 1px solid black;
    text-align: center;
`

const TdCell = styled.td`
    padding: 0;
    border: 1px solid black;
    text-align: center;
`

class AnnouncesDetails extends React.Component { 
    render(){
        let isNorthSouthInZone = this.props.game.zones[0] && this.props.game.zones[2];

        return(
            <TableWrapper>
                <AnnounceTable>
                    <TableHead>
                        <tr>
                            <ThCell isInZone={isNorthSouthInZone}>Username1</ThCell>
                            <ThCell isInZone={!isNorthSouthInZone}>Username2</ThCell>
                            <ThCell isInZone={isNorthSouthInZone}>Username3</ThCell>
                            <ThCell isInZone={!isNorthSouthInZone}>Username4</ThCell>
                        </tr>
                    </TableHead>
                    <TableBody>
                        <tr>
                            <TdCell>Pass</TdCell>
                            <TdCell>1 &hearts;</TdCell>
                            <TdCell>2 &spades;</TdCell>
                            <TdCell>4 &hearts;</TdCell>
                        </tr>
                        <tr>
                            <TdCell>Pass</TdCell>
                            <TdCell>Pass</TdCell>
                            <TdCell>Pass</TdCell>
                            <TdCell></TdCell>
                        </tr>
                    </TableBody>
                </AnnounceTable>                    
            </TableWrapper>
        );
    }
}

const  mapStateToProps = state => {
    return {
      game: state.games.game[0]
    };
}

export default connect(mapStateToProps)(AnnouncesDetails);