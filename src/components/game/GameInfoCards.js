import React from 'react'
import { connect } from 'react-redux';
import GameInfoCardView from './GameInfoCardView';
import { Row } from 'react-bootstrap';

class GameInfoCards extends React.Component {
    render(){
        return(
            <React.Fragment>
                <h2>Cards</h2>

                <Row>
                    <GameInfoCardView side="North" cards={this.props.northCards}></GameInfoCardView>
                    <GameInfoCardView side="East" cards={this.props.eastCards}></GameInfoCardView>
                    <GameInfoCardView side="South" cards={this.props.southCards}></GameInfoCardView>
                    <GameInfoCardView side="West" cards={this.props.westCards}></GameInfoCardView>
                </Row> 
            </React.Fragment>

        )
    }
}

const mapStateToProps = state => {
    let gameCards = state.games.game.GameCards
    let northCards = [];
    let eastCards = [];
    let southCards = [];
    let westCards = [];

    for(let i = 0; i< gameCards.length; i++){
        let card = gameCards[i];
        if(card.Direction === 0){
            northCards.push(card);
        } else if (card.Direction === 1){
            eastCards.push(card);
        } else if (card.Direction === 2){
            southCards.push(card);
        } else if(card.Direction === 3){
            westCards.push(card);
        }
    }

    return { northCards, eastCards, southCards, westCards};
}

export default connect(mapStateToProps)(GameInfoCards);