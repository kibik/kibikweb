import React from 'react'
import { Container } from 'react-bootstrap';
import { connect } from 'react-redux';
import { getGame } from '../../redux/actions';
import GameInfoCards from './GameInfoCards';
import GameInfoAnnounces from'./GameInfoAnnounces';
import BoardDetails from './BoardDetails';
import GameInfoResults from './GameInfoResults';

class GameInfo extends React.Component {
    componentDidMount() { 
        const { gameId } = this.props.match.params; 
        this.props.getGame(gameId);
    }    

    render(){ 
        if (!this.props.game) {
            return "No game to display."       
        }

        return (
            <Container>
                <hr></hr>
                <div>
                    <h2>Board details</h2>
                    <BoardDetails></BoardDetails>
                </div>

                <hr></hr>
                <GameInfoCards></GameInfoCards>

                <hr></hr>
                <GameInfoAnnounces></GameInfoAnnounces>

                <hr></hr>
                <GameInfoResults></GameInfoResults>
                
                <hr></hr>
            </Container>
        )
    }
}

const mapStateToProps = state => {
    console.log(state)
    return { game : state.games.game};
}

export default connect(mapStateToProps, { getGame })(GameInfo);