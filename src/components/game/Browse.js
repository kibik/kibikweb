import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { Container, Row } from 'react-bootstrap';
import TournamentCard from './TournamentCard';
import { getAllTournaments, getTournamentPicture } from '../../redux/actions';
import _ from 'lodash';
import Loading from '../../components/Loading';

class Browse extends React.Component {
    componentDidMount() {        
        this.props.getAllTournaments(true);
    }

    renderGames = () => {
        if(_.isEmpty(this.props.tournaments)){
            return (<Loading></Loading>)
        }

        return this.props.tournaments.map(tournament => {
            return (
                <TournamentCard tournamentId={tournament.Id} key={tournament.Id}></TournamentCard>
            );
        });
    }

  render() {
    return (
        <Container>
            <Row>
                {this.renderGames()}
            </Row>
        </Container>
    );
  }  
};

const mapStateToProps = state => {
    return { tournaments: state.tournaments.tournaments };
}

const mapDispatchToProps = (dispatch) => {
    return {
        ...bindActionCreators({ getAllTournaments }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Browse);