import React from 'react'
import posed from "react-pose";
import styled from 'styled-components'
import { GetCardDimensions } from '../../helper';

const CardWidth = 225 * 0.3;
const CardHeight = 315 * 0.3;

const StyledCard = styled(posed.div({
    bottom: { y: 100, x: 30}
}))`
    position:relative;
    width: ${CardWidth}px;
    height: ${ props => props.half ? CardHeight  : CardHeight }px;
    background: url(/images/cards-sprite.jpg) ${props => props.width * 0.3}px ${props => props.height * 0.3}px;
    border-radius: 5px;
    background-size: ${2925 * 0.3}px ${1260 * 0.3}px;
    z-index: ${props => props.index};
    margin-left: ${props => props.half ? (props.index > 0 ? -48 : 0) : (props.rowIndex > 0 ? -48 : 0)}px;
    margin-top: ${props => props.half ? 0 : props.colorIndex > 0 ? -60 : 0}px;
`

class Card extends React.Component {
    state = {pose: 'top'}

    handleMouseEnter = () => this.setState({
        
      pose: this.state.pose === 'top' ? 'bottom' : 'top'
    })

    render(){
        let {width, height} = GetCardDimensions(this.props.sign, this.props.color);

        return(
            <StyledCard rowIndex={this.props.rowIndex} width={width} height={height} colorIndex={this.props.colorIndex} index={this.props.index} half={this.props.half} isEast={this.props.isEast}  pose={this.state.pose} onClick={this.handleMouseEnter}></StyledCard>
        )
    }
}

export default Card;