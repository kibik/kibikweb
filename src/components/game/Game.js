import React from 'react'
import { Container, Row, Col, Form, FormGroup, FormControl, Button  } from 'react-bootstrap';
import { connect } from 'react-redux';
import Comment from './Comment';
import Table from './Table';
import _ from 'lodash';
import { fetchGame, fetchComments } from '../../redux/actions';
import styled from 'styled-components';

const Dashboard = styled.div`
    margin: 5px 0 0 0;
`

const CommentBox = styled.div`
    border: 1px solid #bbb;
    //padding-bottom: 2px;
    border-radius: 5px;
`

const TitleBox = styled.div`
    background-color: #c2d6d6;
    padding:10px;
`

const ActionBox = styled.div`
    padding:10px;
`

const CommentList = styled.ul`
    padding: 0;
    list-style: none;
    max-height: 300px;
    overflow: scroll;
    overflow-x: hidden;
`

const StyledForm = styled(Form)`
    padding: 2px;
`
class Game extends React.Component {
    componentDidMount() { 
        const { gameId } = this.props.match.params; 

        this.props.fetchGame(gameId);
        this.props.fetchComments(gameId);
    }

    renderComments = () => {
        let comments = this.props.comments;
        if(_.isEmpty(comments)){
            return (<p>Loading..</p>)
        }

        return comments.map(comment => {
            return (
                <Comment comment={comment} key={comment.id}></Comment>
            )
        });
    }
    
    render(){ 
        return (
            <Container>
                <Dashboard>
                    <Row>
                        <Col md="9">
                            <Table game={this.props.game}></Table>
                        </Col>
                        <Col md="3">
                            <CommentBox>
                                <TitleBox>
                                    <label>Comments</label>
                                </TitleBox>
                                <ActionBox>
                                    <CommentList>
                                        { this.renderComments()} 
                                    </CommentList>
                                </ActionBox>
                                <StyledForm inline={true}>
                                    <FormGroup>
                                        <FormControl type="text" placeholder="Enter comment"></FormControl>
                                    </FormGroup>
                                    <FormGroup>
                                        <Button variant="secondary" size="sm">Add</Button>
                                    </FormGroup>
                                </StyledForm>
                            </CommentBox>
                        </Col>
                    </Row>
                </Dashboard>
            </Container>
        )
    }
}

const mapStateToProps = state => {
    return { game : state.games.game, comments: state.games.comments };
}

export default connect(mapStateToProps, { fetchGame, fetchComments })(Game);