import React from 'react'
import { Container } from 'react-bootstrap';
import { connect } from 'react-redux';
import { getAllGamesByTournamentId } from '../../redux/actions';
import styled from 'styled-components';
import DataTable from 'react-data-table-component';
import { Link } from 'react-router-dom'

const columns = [
    {
      name: 'Round #',
      selector: 'sequenceNumber',
      sortable: true,
    }
  ];

const StyledDataTable = styled.div`
    height: 100%;
    overflow-y: hidden; 
`;

const ExpandedSection = ({data}) => {
    if(data.finishedGames.length === 0){
        return (<div>No finished games yet</div>)
    }

    return (       
        <ul>
            {
                data.finishedGames.map(
                    (game) => (
                        <li key={game.id}>
                            Board: {game.board.sequence}, Contract: {game.contract} - <Link className="btn btn-light" key={"watch" + game.id} to={"/browse/tournament/watch/" + game.id}>Watch</Link>
                        </li>
                    )
                )
            }
        </ul>
    )
};

class WatchPlayedGames extends React.Component {
    constructor(props) {
        super(props);

        this.state = { 
            selectedRows: [], 
            toggleCleared: false 
        };
    }

    handleChange = state => {
        this.setState({ selectedRows: state.selectedRows });
      };

    handleRowClicked = row => {
        console.log(`${row.Name} was clicked!`);
      };
    
    componentDidMount() { 
        const { tournamentId } = this.props.match.params; 
        this.props.getAllGamesByTournamentId(tournamentId);
    }    

    render(){ 
        if (!this.props.rounds) {
            return "No games to display."       
        }

        return (
            <Container>
                <StyledDataTable>
                <DataTable
                    title="Finished Games By Rounds"
                    columns={columns}
                    data={this.props.rounds}
                    keyField="Id"
                    selectableRows 
                    highlightOnHover
                    defaultSortField="sequenceNumber"
                    //actions={actions}
                    //contextActions={contextActions}
                    onTableUpdate={this.handleChange}
                    clearSelectedRows={this.state.toggledClearRows}
                    onRowClicked={this.handleRowClicked}
                    pagination
                    expandableRows
                    expandableRowsComponent={<ExpandedSection tournamentId={this.props.match.tournamentId} />}
                />
            </StyledDataTable>  
            </Container>
        )
    }
}

const mapStateToProps = state => {
    return { rounds : state.games.rounds};
}

export default connect(mapStateToProps, { getAllGamesByTournamentId })(WatchPlayedGames);