import React from 'react'
import { connect } from 'react-redux';

class GameInfoCards extends React.Component {
    render(){
        return(
            <React.Fragment>
                <h2>Results</h2>

                <p>Contract: {this.props.game.Contract}</p>
                <p>Attack: {this.props.game.Attack}</p>
                <p>North/South tricks: {this.props.game.NorthSouthTricks}</p>
                <p>East/West tricks: {this.props.game.EastWestTricks}</p>
                <p>North/South result: {this.props.game.NorthSouthResult}</p>
                <p>East/West result: {this.props.game.EastWestResult}</p>
            </React.Fragment>

        )
    }
}

const mapStateToProps = state => {
    return { game: state.games.game};
}

export default connect(mapStateToProps)(GameInfoCards);