import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Image, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';
import tournamentDefault from '../../images/tournament-default.jpg';
import { 
    getTournamentById
} from '../../redux/actions';

const Center = styled.div`
    text-align: center;
`;

const TournamentName = styled.h1`
    margin-top: 25px;
    margin-bottom: 50px;
`;

const WatchButton = styled(Link)`
    display: block;
    width: 20%;
    margin: auto;
    margin-bottom: 10px;
`;

const TournamentDescription = styled.div`
    text-align: justify;
    margin-top: 15px;
    border-width: .2rem;
    border: solid #f7f7f9;
    padding: 1.5rem;
`

class TournamentDetails extends React.Component {
    componentDidMount(){
        const { tournamentId } = this.props.match.params; 
        this.props.getTournamentById(tournamentId, true);
    }

    renderPicture = () => {
        if(this.props.tournament.Logo){
            return this.props.tournament.Logo;
        }

        return tournamentDefault;
    }

    renderGameDetails = () => {
        if(!this.props.tournament) {
            return (<div>Loading</div>)
        }

        let { Name, Description } = this.props.tournament;
        return (
            <Fragment>
                <Center>
                    <TournamentName>{Name}</TournamentName>
                    <WatchButton className="btn btn-outline-info" to={ this.props.match.url + "/watch" }>Watch</WatchButton>
                    <Image src={this.renderPicture()} thumbnail />
                  
                </Center>

                <TournamentDescription><p>{ Description }</p></TournamentDescription>
            </Fragment>
        )
    }

    render() {
        return(
            <Container>
                { this.renderGameDetails() }
            </Container>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    let tournament = {};
   
    if(state.tournaments.tournaments){
        const { tournamentId } = ownProps.match.params; 
        tournament = state.tournaments.tournaments.find(t => t.Id === parseInt(tournamentId));
        return { 
            tournament
        };
    } else if (state.tournaments.tournament){
        return { 
            tournament : state.tournaments.tournament
        };
    }

    return tournament;
}

const mapDispatchToProps = (dispatch) => {
    return {
        ...bindActionCreators({ getTournamentById }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TournamentDetails);