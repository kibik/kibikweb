import React from 'react';
import styled from 'styled-components';

const CommentDetails = styled.div`
  margin:0;
  margin-top:10px;
`

const ProfilePicture = styled.div`
  width: 40px;
  margin-right:5px;
  height: 40px;
  float: left;
`

const Picture = styled.img`
  width:100%;
  height: 100%;
  border-radius:50%;
`

const CommentText = styled.p`
  margin:0;
`

const Author = styled.span`
  font-weight: bold;
  margin-right: 5px;
`

const SubText = styled.p`
  color:#aaa;
  font-family:verdana;
  font-size:11px;
`

class Comment extends React.Component {
    render(){
        let { comment } = this.props;

        return(
          <>
            <CommentDetails>
              <ProfilePicture>
                <Picture src={comment.profilePicture} alt={comment.author}></Picture>
              </ProfilePicture>
              <div>
                <CommentText>
                  <Author>{comment.author}</Author>{comment.content}
                </CommentText>
                <SubText>
                  on March 5th, 2014
                </SubText>
              </div>
            </CommentDetails>
          </>
        )
    }
}

export default Comment;