import React from 'react';
import styled from 'styled-components';
import { connect } from "react-redux";

const Details = styled.div`
    display: flex;
    height: 100%;
    padding: 5px;
    margin-right: 10px;
`

const DetailsContent = styled.div`
    background-color: #00ff99;
    display: inline-block;
    align-self: flex-end;
    padding: 8px;
    width: 100%;
    border-radius: 5px;
`

const TextLeft = styled.div`
    font-weight: bold
    float: left;
`

const TextRight = styled.div`
    font-weight: bold
    float: right;
`

class ContractDetails extends React.Component {
    render() {
        return (
            <Details>
                <DetailsContent>
                    <TextLeft>4 &hearts; E</TextLeft>
                    <TextRight>EW 2 NS 1</TextRight>    
                </DetailsContent>
            </Details>
        );
    }
}

const  mapStateToProps = state => {
    return {
      game: state.games.game[0]
    };
}

export default connect(mapStateToProps)(ContractDetails);