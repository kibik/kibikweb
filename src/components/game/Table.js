import React from 'react';
import _ from 'lodash';
import styled from 'styled-components';
import Card from './Card';
import BoardDetails from './BoardDetails';
import ContractDetails from './ContractDetails';
import AnnouncesDetails from './AnnouncesDetails';
import { Grid, Cell } from 'styled-css-grid';
import { GetSideRotation } from '../../helper';

const Board = styled.div`
    background: linear-gradient(110deg, #fdcd3b 60%, #ffed4b 60%);
    padding: 5px;
    border: solid 3px red;
    border-radius: 5px;
`

const Side = styled.div`
    -ms-transform: rotate(${props => props.rotation}deg); /* IE 9 */
    -webkit-transform: rotate(${props => props.rotation}deg); /* Safari */
    transform: rotate(${props => props.rotation}deg); /* Standard syntax */
    display:flex;
    flex-direction:${props => props.half ? 'row' : 'column'};
    justify-content: center;
    align-items: ${props => props.isEast ? 'flex-end' : 'flex-start'};
    margin-right: ${props => props.isEast ? 12 : 0 }px;
`

const ColorRow = styled.div`
    display:flex;
    flex-direction: row;
`

class Table extends React.Component {
    renderPlayerCards = (cards, isHalf, isEast) => {
        let index = 0;
        return cards.map((card, colorIndex) => {
            let signs = _.values(card)[0];

            return (
                <ColorRow key={colorIndex} half={isHalf}>{ signs.map((sign, rowIndex) => {
                return(
                    <Card key={`${sign}_${_.keys(card)[0]}`} sign={sign} color={_.keys(card)[0]} colorIndex={colorIndex} index={index++} rowIndex={rowIndex} half={isHalf} isEast={isEast}></Card>
                )
                }) }</ColorRow>
            )
        })
    }

    renderCards = (game) => {
        return game.map(g => {
            let side = g.currentSide;
            switch(side){
                case "N":
                    return (
                        <Cell key={side} area={side}>
                            <Side rotation={GetSideRotation(side)} half={true}>
                                { this.renderPlayerCards(g.cards, true, false) }
                            </Side>
                        </Cell>
                    );
                case "E":
                    return (
                        <Cell key={side} area={side} >
                            <Side half={false} isEast={true}>
                                { this.renderPlayerCards(g.cards, false, true) }
                            </Side>
                        </Cell>
                    );
                case "S":
                    return (
                        <Cell key={side} area={side}>
                            <Side rotation={GetSideRotation(side)} half={true}>
                                { this.renderPlayerCards(g.cards, true, false) }
                            </Side>
                        </Cell>
                    );  
                case "W":
                    return (
                        <Cell key={side} area={side}>
                            <Side half={false}>
                                { this.renderPlayerCards(g.cards, false, false) }
                            </Side>
                        </Cell>
                    );
                default:
                    throw new Error(`Not supported side. The provided values is ${side}`);
            }
        })
    }

    setupTable = () => {
        let game = this.props.game;
        if(_.isEmpty(game) || !game){
            return (<p>Loading..</p>)
        }

        return (
            <Grid
                columns={"240px 2px 305px 2px 240px"}
                rows={"100px 5px 200px 5px 100px"}
                areas={[
                    "BoardDetails   .   N   .   Announces",
                    ".   .   .   .  .",
                    "W   .   .   .  E",
                    ".   .   .   .  .",
                    ".   .   S   .   Contract"
                ]}
                alignContent="start">

                <Cell area="BoardDetails"><BoardDetails></BoardDetails></Cell>
                <Cell area="Announces"><AnnouncesDetails></AnnouncesDetails></Cell>
                <Cell area="Contract"><ContractDetails></ContractDetails></Cell>

                { this.renderCards(this.props.game[0].game)}
            </Grid>
        )
    }

    render(){
        return(
            <Board>           
               { this.setupTable() }
            </Board>
        )
    }
}

export default Table;