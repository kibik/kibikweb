import React from 'react';
import styled from 'styled-components';
import { Grid, Cell } from 'styled-css-grid';
import { connect } from "react-redux";

const Side = styled(Cell)`
    border-radius: 3px;
    text-align: center;
    vertical-align: middle;
    background-color: ${props => props.isInZone ? 'red' : 'green'}; 
    color: ${props => props.isInZone ? 'white' : 'black'} 
`

const Text = styled.span`
    margin: auto; 
    font-weight: bold;
`

const Center = styled(Cell)`
    background-color: #00ffff;
    border-radius: 3px;
    padding: 2px;
    text-align: center;
    vertical-align: middle;
`

class BoardDetails extends React.Component {
    checkIfDealer = (side, currentSide) => {
        if(side === currentSide){
            return <Text>D</Text>;
        }
    };

    render(){
        let {IsNorthSouthInZone, Sequence, Dealer} = this.props.board;

        return(
            <Grid
                columns={"25px 120px 25px"}
                rows={"22px 40px 22px"}
                areas={[
                    ".   N   .",
                    "W   Center  E",
                    ".   S   ."
                ]}>

                <Side area="N" isInZone={IsNorthSouthInZone}>{ this.checkIfDealer(Dealer, 0) }</Side>
                <Side area="E" isInZone={ !IsNorthSouthInZone}>{ this.checkIfDealer(Dealer, 1) }</Side>
                <Side area="S" isInZone={IsNorthSouthInZone}>{ this.checkIfDealer(Dealer, 2) }</Side>
                <Side area="W" isInZone={ !IsNorthSouthInZone}>{ this.checkIfDealer(Dealer, 3) }</Side>

                <Center area="Center"><Text>Board {Sequence}</Text></Center>
            </Grid>
        )
    }
}

const  mapStateToProps = state => {
    return {
        board: state.games.game.Board
    };
}

export default connect(mapStateToProps)(BoardDetails);