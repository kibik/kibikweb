import React from 'react'
import _ from 'lodash';
import { Table } from 'react-bootstrap';
import { connect } from 'react-redux';

class GameInfoAnnounces extends React.Component {

    getRow = (announces, rowsCount) => {
        let localCounter = 0;
        return (       
            <tr key={rowsCount}>
                {
                    announces.map(
                        (announce) => {
                            if(_.isEmpty(announce)){
                                return (
                                    <td key={`${rowsCount}row-${localCounter++}`}>
                                       -
                                    </td>
                                )
                            } else {
                                return (
                                    <td key={announce.Id}>
                                        {announce.AnnounceTypeString}
                                    </td>
                                )
                                
                            }
                        } 
                    )
                }
            </tr>
        );
    }

    fillInTable = (announces) => {
        let i = 0;
        let row = [];
        let rowCount = 0;
        let currentRow = 0;//used only as a key for <tr> 

        let output = [];
        while(true){
            if(i >= announces.length && rowCount === 4){
                output.push(this.getRow(row, currentRow));
                break;
            }

            if(rowCount === 4){
                output.push(this.getRow(row, currentRow));
                rowCount = 0;
                row = [];
                currentRow++;
            }

            if(announces[i]){
                if(rowCount === (announces[i].SequenceNumber % 4) ){
                    row.push(announces[i]);
                    i++;
                } else{
                    row.push({});
                }
            } else {
                row.push({});
            }

            rowCount++;
        }

        return (output);
    }

    render(){
        return(
            <React.Fragment>
                <h2>Announces</h2>

                <Table>
                    <thead>
                        <tr>
                            <th>North</th>
                            <th>East</th>
                            <th>South</th>
                            <th>West</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.fillInTable(this.props.announces)}
                    </tbody>
                </Table>
            </React.Fragment> 
        )
    }
}

const mapStateToProps = state => {
    let announces = state.games.game.GameAnnounces.sort((a, b) => parseInt(a.SequenceNumber) - parseInt(b.SequenceNumber));
    return { announces : announces};
}

export default connect(mapStateToProps)(GameInfoAnnounces);