import React from 'react'
import { Col } from 'react-bootstrap';

class GameInfoCardView extends React.Component {
    render(){
        let gameCards = this.props.cards;
        let spades = [];
        let hearths = [];
        let diamonds = [];
        let clubs = [];

        for(let i = 0; i < gameCards.length; i++){
            let card = gameCards[i].CardTypeString.slice(-2);
            if(card === "Sp"){
                spades.push(gameCards[i]);
            } else if (card === "He"){
                hearths.push(gameCards[i]);
            } else if (card === "Di"){
                diamonds.push(gameCards[i]);
            } else if(card === "Cl"){
                clubs.push(gameCards[i]);
            }
        }

        return(
            <Col md={3}>
                <h4>{this.props.side}</h4>

                <ul>
                    {
                        spades.map(
                            (card) => (
                            <li key={card.Id}>
                                {card.CardTypeString}
                            </li>
                        ))
                    }
                </ul> 

                <ul>
                    {
                        hearths.map(
                            (card) => (
                            <li key={card.Id}>
                                {card.CardTypeString}
                            </li>
                        ))
                    }
                </ul> 

                <ul>
                    {
                        diamonds.map(
                            (card) => (
                            <li key={card.Id}>
                                {card.CardTypeString}
                            </li>
                        ))
                    }
                </ul> 

                <ul>
                    {
                        clubs.map(
                            (card) => (
                            <li key={card.Id}>
                                {card.CardTypeString}
                            </li>
                        ))
                    }
                </ul> 
            </Col>
        )
    }
}

export default GameInfoCardView;