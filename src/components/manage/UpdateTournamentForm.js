import React from 'react'
import { bindActionCreators } from 'redux'
import {connect} from 'react-redux';
import TournamentForm from './childrenComponents/TournamentForm';
import TournamentPicture from './childrenComponents/TournamentPicture';

import { 
    getTournamentById,
    submitUpdateTournamentForm 
} from '../../redux/actions';

class UpdateTournamentForm extends React.Component {
    componentDidMount(){
        const { tournamentId } = this.props.match.params; 
        this.props.getTournamentById(tournamentId, true);
    }

    onFormSubmit = (formValues) => {
        let { tournamentId } = this.props.match.params; 
        formValues.tournamentId = tournamentId;
        this.props.submitUpdateTournamentForm(formValues);
    }

    render(){
        if (!this.props.initialValues.Name) return null;

        return(
            <React.Fragment>
                <div><h2>Update Tournament</h2></div>

                <TournamentPicture tournamentId={this.props.initialValues.Id}></TournamentPicture>

                <TournamentForm 
                    onSubmit={this.onFormSubmit}
                    initialValues={this.props.initialValues}
                    buttonName="Update"
                >
                </TournamentForm>
            </React.Fragment>);
    }
}

const mapStateToProps = state => {
    let initialValues = state.tournaments.tournament;
    if (!initialValues) { 
        return { 
            initialValues: {dates: [{ StartDate : new Date(), EndDate : new Date() }] } 
        }
    } 

    initialValues.Type = initialValues.Type.toString();
    initialValues.dates = [{ StartDate : new Date(initialValues.StartDate), EndDate : new Date(initialValues.EndDate) }];
    return { 
        initialValues,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        ...bindActionCreators({ getTournamentById, submitUpdateTournamentForm }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdateTournamentForm)