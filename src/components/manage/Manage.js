import React from 'react';
import styled from 'styled-components';
import { Row, Col, Nav } from 'react-bootstrap';
import CreateClubForm from './CreateClubForm';
import { Link, Route } from 'react-router-dom'
import CreateTournamentForm from './CreateTournamentForm';
import ManageTournaments from './ManageTournaments';
import ManageLocations from './ManageLocations';
import UpdateTournamentForm from './UpdateTournamentForm';
import GeneralManageTournamentForm from './GeneralManageTournamentForm';
import CreateLocationForm from './CreateLocationForm';
import UpdateLocationForm from './UpdateLocationForm';
import ManageClubs from './ManageClubs';
import UpdateClubForm from './UpdateClubForm';
import CreatePlayerForm from './CreatePlayerForm';
import ManagePlayers from './ManagePlayers';
import UpdatePlayerForm from './UpdatePlayerForm';

const Sidebar = styled.div`
     background: #b8b894;
     height: 100vh;
`

const SidebarLink = styled(Link)`
    display: block;
    padding: 0.75rem 1.5rem;
    font-weight: 400;
    transition: background .1s ease-in-out;
    position: relative;
    text-decoration: none;
    cursor: pointer;
    color: black;
    
    &:hover {
        color: #ced4da;
        background: #4d4d33
    }
`

const Main = styled.div`
    margin: 0;
    padding: 0;
`

class Manage extends React.Component {
    render(){
        return (
            <React.Fragment>
                 <Row>
                    <Col md="2" offset="1">
                        <Sidebar>
                            <Nav className="flex-column">
                                <SidebarLink to="/manage/locations" className="nav-link">Locations</SidebarLink>
                                <SidebarLink to="/manage/clubs" className="nav-link">Clubs</SidebarLink>
                                <SidebarLink to="/manage/tournaments" className="nav-link">Tournaments</SidebarLink>
                                <SidebarLink to="/manage/players" className="nav-link">Players</SidebarLink>
                            </Nav>
                        </Sidebar>
                    </Col>
                    <Col md="8" offset="1">
                        <Main>
                            <Route path='/manage/locations' exact component={ ManageLocations } />
                            <Route path='/manage/location' exact component={ CreateLocationForm } />
                            <Route path='/manage/location/:locationId' exact component={ UpdateLocationForm } />

                            <Route path='/manage/clubs' exact component={ ManageClubs } />
                            <Route path='/manage/club' exact component={ CreateClubForm } />
                            <Route path='/manage/club/:clubId' exact component={ UpdateClubForm } />

                            <Route path='/manage/tournament' exact component={ CreateTournamentForm } />
                            <Route path='/manage/tournament/update/:tournamentId' exact component={ UpdateTournamentForm } />
                            <Route path='/manage/tournament/general/:tournamentId' exact component={ GeneralManageTournamentForm } />
                            <Route path='/manage/tournaments' exact component={ ManageTournaments } />

                            <Route path='/manage/players' exact component={ ManagePlayers } />
                            <Route path='/manage/player' exact component={ CreatePlayerForm } />
                            <Route path='/manage/player/:playerId' exact component={ UpdatePlayerForm } />
                        </Main>
                    </Col>
                </Row> 
            </React.Fragment>
        )
    }
}

export default Manage;