import React from 'react'
import {Button} from 'react-bootstrap';
import styled from 'styled-components';
import DataTable from 'react-data-table-component';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { getAllLocations, deleteLocations } from '../../redux/actions';
import { Link } from 'react-router-dom'

const columns = [
  {
    name: 'Name',
    selector: 'name',
    sortable: true,
  },
  {
    name: 'Address',
    selector: 'address',
    sortable: true,
  },
  {
    name: 'City',
    selector: 'city',
    sortable: true,
  },
  {
    name: 'Country',
    selector: 'country',
    sortable: true,
  },
  {
    cell: (row) => (
      <Link className="btn btn-light" key={"update" + row.id} to={"/manage/location/" + row.id} >Update</Link>
    ),
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
  }
];

const StyledDataTable = styled.div`
    height: 100%;
    overflow-y: hidden; 
`;

class ManageLocations extends React.Component {
    constructor(props) {
        super(props);

        this.state = { 
            selectedRows: [], 
            toggleCleared: false 
        };
    }

    componentDidMount() {
        this.props.getAllLocations();
    }

    handleChange = state => {
        this.setState({ selectedRows: state.selectedRows });
      };

    handleRowClicked = row => {
        console.log(`${row.name} was clicked!`);
      };
    
    deleteAll = () => {
        const { selectedRows } = this.state;
        const rows = selectedRows.map(r => r.name);
    
        if (window.confirm(`Are you sure you want to delete:\r ${rows}?`)) {
          this.setState(state => ({
            toggleCleared: !state.toggleCleared
          }));

          this.props.deleteLocations(this.state.selectedRows);
        }
      };
      
    render() {
        const actions = [
            <Link className="btn btn-light" key="add" to="/manage/location">Add</Link>,
        ];
        
        const contextActions = [
            <Button key="delete" onClick={this.deleteAll}>
              Delete
            </Button>,
        ];

        return (
            <StyledDataTable>
                <DataTable
                    title="Manage Locations"
                    columns={columns}
                    data={this.props.locations}
                    keyField="Id"
                    selectableRows 
                    highlightOnHover
                    defaultSortField="Name"
                    actions={actions}
                    contextActions={contextActions}
                    onTableUpdate={this.handleChange}
                    clearSelectedRows={this.state.toggledClearRows}
                    onRowClicked={this.handleRowClicked}
                    pagination
                />
            </StyledDataTable>         
        )
    }
}

const mapStateToProps = state => {
    return { 
        locations: state.locations.locations
    };
}

const mapDispatchToProps = (dispatch) => {
  return {
      ...bindActionCreators({ deleteLocations, getAllLocations }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageLocations);