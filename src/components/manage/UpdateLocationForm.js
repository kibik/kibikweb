import React from 'react'
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { submitUpdateLocationForm, getLocationById } from '../../redux/actions';
import LocationForm from './childrenComponents/LocationForm';

class UpdateLocationForm extends React.Component {
    componentDidMount(){
        const { locationId } = this.props.match.params; 
        this.props.getLocationById(locationId);
    }

    onFormSubmit = (formValues) => {
        let { locationId } = this.props.match.params; 
        //formValues.id = locationId;
        this.props.submitUpdateLocationForm(formValues);
    }

    render(){
        return(
            <React.Fragment>
                <div><h2>Update Location</h2></div>

                <LocationForm 
                    onSubmit={this.onFormSubmit} 
                    buttonName="Update" 
                    initialValues={this.props.initialValues}>      
                </LocationForm>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    let initialValues = state.locations.location;
    return { 
        initialValues,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        ...bindActionCreators({ getLocationById, submitUpdateLocationForm }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdateLocationForm)