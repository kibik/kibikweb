import React from 'react'
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import ManageTournamentForm from './childrenComponents/ManageTournamentForm';
import { 
    getTournamentById,
    getTournamentPairs,
} from '../../redux/actions';

class GeneralManageTournamentForm extends React.Component {
    componentDidMount(){
        const { tournamentId } = this.props.match.params;
        const pictureIsRequired = false;
        this.props.getTournamentById(tournamentId, pictureIsRequired);
        this.props.getTournamentPairs(tournamentId);
    }

    onFormSubmit = (formValues) => {
        console.log(formValues);
        //this.props.submitLocationForm(formValues);
    }

    render(){
        return(
            <React.Fragment>
                <div><h2>Manage Tournament</h2></div>
                <br></br>
                <ManageTournamentForm onSubmit={this.onFormSubmit}
                initialValues={this.props.initialValues}>
                </ManageTournamentForm>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => {
    let initialValues = {};
    initialValues.tournament = state.tournaments.tournament;
    initialValues.tournamentPairs = state.manage.tournamentPairs;
    return {
        initialValues
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        ...bindActionCreators({ getTournamentById, getTournamentPairs }, dispatch)
    }
}

export default connect(
        mapStateToProps, 
        mapDispatchToProps
    )(GeneralManageTournamentForm)