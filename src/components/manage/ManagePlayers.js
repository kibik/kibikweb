import React from 'react'
import styled from 'styled-components';
import DataTable from 'react-data-table-component';
import differenceBy from 'lodash/differenceBy';
import {connect} from 'react-redux';
import { getAllPlayers } from '../../redux/actions';
import { Link } from 'react-router-dom'

const columns = [
  {
    name: 'First Name',
    selector: 'firstName',
    sortable: true,
  },
  {
    name: 'Last Name',
    selector: 'lastName',
    sortable: true,
  },
  {
    name: 'Country',
    selector: 'country',
    sortable: true,
  },
  {
    name: 'IBN',
    selector: 'ibn',
    sortable: true,
  },
  {
    cell: (row) => (
      <Link className="btn btn-light" key={"update" + row.id} to={"/manage/player/" + row.id} >Update</Link>
    ),
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
  }
];

const StyledDataTable = styled.div`
    height: 100%;
    overflow-y: hidden; 
`;

class ManagePlayers extends React.Component {
    constructor(props) {
        super(props);

        this.state = { 
            selectedRows: [], 
            toggleCleared: false 
        };
    }

    componentDidMount() {
        this.props.getAllPlayers();
    }

    handleChange = state => {
        this.setState({ selectedRows: state.selectedRows });
      };

    handleRowClicked = row => {
        console.log(`${row.name} was clicked!`);
      };
    
    // deleteAll = () => {
    //     const { selectedRows } = this.state;
    //     const rows = selectedRows.map(r => r.name);
    
    //     if (window.confirm(`Are you sure you want to delete:\r ${rows}?`)) {
    //       this.setState(state => ({
    //         toggleCleared: !state.toggleCleared,
    //         data: differenceBy(state.data, state.selectedRows, 'name'),
    //       }));
    //     }
    //   };

    // deleteOne = row => {
    //     if (window.confirm(`Are you sure you want to delete:\r ${row.name}?`)) {
    //       const { data } = this.state;
    //       const index = data.findIndex(r => r === row);
    
    //       this.setState(state => ({
    //         toggleCleared: !state.toggleCleared,
    //         data: [...state.data.slice(0, index), ...state.data.slice(index + 1)],
    //       }));
    //     }
    //   };
      
    render() {
        const actions = [
            <Link className="btn btn-light" key="add" to="/manage/player">Add</Link>,
        ];
        
        // const contextActions = [
        //     <Button key="delete" onClick={this.deleteAll} style={{ color: 'red' }} icon>
        //       delete
        //     </Button>,
        //   ];

        return (
            <StyledDataTable>
                <DataTable
                    title="Manage Players"
                    columns={columns}
                    data={this.props.players}
                    keyField="id"
                    selectableRows 
                    highlightOnHover
                    defaultSortField="FirstName"
                    actions={actions}
                    //contextActions={contextActions}
                    onTableUpdate={this.handleChange}
                    clearSelectedRows={this.state.toggledClearRows}
                    onRowClicked={this.handleRowClicked}
                    pagination
                />
            </StyledDataTable>         
        )
    }
}

const mapStateToProps = state => {
    return { 
        players: state.players.players
    };
}

export default connect(mapStateToProps, { getAllPlayers })(ManagePlayers);