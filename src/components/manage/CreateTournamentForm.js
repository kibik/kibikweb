import React from 'react'
import {connect} from 'react-redux';
import { submitTournamentForm } from '../../redux/actions';
import TournamentForm from './childrenComponents/TournamentForm';

class CreateTournamentForm extends React.Component {
    onFormSubmit = (formValues) => {       
        if (formValues.dates){
            formValues.StartDate = formValues.dates[0].StartDate;
            formValues.EndDate = formValues.dates[0].EndDate;
        }
        
        this.props.submitTournamentForm(formValues);
    }

    render(){
        return(
            <React.Fragment>
                <div><h2>Create Tournament</h2></div>

                <TournamentForm 
                    onSubmit={this.onFormSubmit}
                    initialValues={{
                        dates: [{ StartDate : new Date(), EndDate : new Date() }] 
                    }}
                    buttonName="Create"
                >
                </TournamentForm>
            </React.Fragment>);
    }
}

export default connect(null, {submitTournamentForm})(CreateTournamentForm)