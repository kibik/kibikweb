import React from 'react'
import {connect} from 'react-redux';
import { submitClubForm } from '../../redux/actions';
import ClubForm from './childrenComponents/ClubForm';

class CreateClubForm extends React.Component {    
    onFormSubmit= (formValues) => {
        this.props.submitClubForm(formValues);
    }

    render() {
        return(
            <React.Fragment>
                <div>
                    <h2>Create Club</h2>
                </div>
                
                <ClubForm 
                    onSubmit={this.onFormSubmit} 
                    buttonName="Create"
                    initialValues={this.props.initialValues}
                >
                </ClubForm>
            </React.Fragment>
        );
    }
}


export default connect(null, { submitClubForm })(CreateClubForm)