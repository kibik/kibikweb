import React from 'react'
import {
    Form,
    Button,
    Table
} from 'react-bootstrap';
import {connect} from 'react-redux';
import {Field, FieldArray, reduxForm, formValueSelector} from 'redux-form';
import SimpleInputFormField from '../../commonComponents/formComponents/SimpleInputFormField';
import SimpleDropdownFormField from '../../commonComponents/formComponents/SimpleDropdownFormField';
import {
    getAllPairs, getTournamentById, getTournamentPairs,
} from '../../../redux/actions';
import styled from "styled-components";
import DataTable from "react-data-table-component";
import {bindActionCreators} from "redux";


const pairsTableColumns = [
    {
        name: 'Name',
        selector: 'name',
        sortable: true,
    },
    {
        name: 'Player1',
        selector: 'player1',
        sortable: true,
    },
    {
        name: 'Player2',
        selector: 'player2',
        sortable: true,
    },
    {
        cell: (row) => (
            <button className="btn btn-light" key={"enroll" + row.Id} onClick={() => {console.log("Should Enroll")}}>Enroll</button>
        ),
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
    }
];

const StyledDataTable = styled.div`
    height: 100%;
    overflow-y: hidden; 
`;

class ManageTournamentForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showPairsTable: true,
            selectedRows: [],
            toggleCleared: false
        };
    }

    componentDidMount(){
        this.props.getAllPairs();
    }

    onFormSubmit = formValues => {
        this.props.onSubmit(formValues);
    };

    renderPairsTable = () => {
        let fields = [];
        if (this.props.initialValues.pairs) {
            console.log(this.props.initialValues.pairs);
            for(let i = 0; i < this.props.initialValues.pairs.length; i++) {
                const pair = this.props.initialValues.pairs[i];

                const pairName = pair.name;
                const player1Name = `${pair.player1.firstName} ${pair.player1.lastName}`;
                const player2Name = `${pair.player2.firstName} ${pair.player2.lastName}`;

                fields.push({Id : pair.id, name: pairName, player1: player1Name, player2: player2Name});
            }
        }

        return (
            <StyledDataTable>
                <DataTable
                    title="All Pairs"
                    columns={pairsTableColumns}
                    data={fields}
                    keyField="Id"
                    selectableRows
                    highlightOnHover
                    defaultSortField="Name"
                    onTableUpdate={this.handleChange}
                    clearSelectedRows={this.state.toggledClearRows}
                    onRowClicked={this.handleRowClicked}
                    pagination
                />
            </StyledDataTable>
        )
    };

    handleChange = state => {
        this.setState({selectedRows: state.selectedRows});
    };

    handleRowClicked = row => {
        console.log(`${row.Id} was clicked!`);
    };

    renderTournamentPairsTable = ({fields}) => {
        if (this.props.initialValues.tournamentPairs) {
            fields = this.props.initialValues.tournamentPairs;
        }

        return (
            <div>
                {
                    <Table
                        bordered
                        responsive
                    >
                        <thead>
                        <tr>
                            <th>Pair #</th>
                            <th>Name</th>
                            <th>Player 1</th>
                            <th>Player 2</th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        {fields.map((member, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{member.pair.name}</td>
                                        <td>
                                            {member.pair.player1.firstName} {member.pair.player1.lastName}
                                        </td>
                                        <td>
                                            {member.pair.player2.firstName} {member.pair.player2.lastName}
                                        </td>
                                        <td>
                                            <Button
                                                type="button"
                                                title="Remove"
                                                onClick={() => {
                                                    this.props.change(`pair#${index}.player1`, "");
                                                    this.props.change(`pair#${index}.player2`, "");
                                                }}
                                            >
                                                Remove
                                            </Button>
                                        </td>
                                    </tr>
                                );
                            }
                        )}
                        </tbody>
                    </Table>}
                {this.renderPairsTable()}
            </div>
        );
    };

    render() {
        return (
            <Form
                onSubmit={this.props.handleSubmit(this.onFormSubmit)}
            >
                <div>
                    <h5>{this.props.initialValues.tournament ? this.props.initialValues.tournament.Name : "Tournament Name"}</h5>
                </div>

                <FieldArray name="members" component={this.renderTournamentPairsTable}
                            initialValues={this.props.initialValues.tournamentPairs}/>

                <Button variant="primary" type="button" onClick={this.renderPairsTable}>
                    Add
                </Button>
            </Form>
        );
    }
}

const mapStateToProps = state => {
    console.log(state);
    return {
        initialValues: {tournamentPairs: state.manage.tournamentPairs, tournament: state.tournaments.tournament, pairs : state.pairs.pairs}
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        ...bindActionCreators({ getTournamentById, getTournamentPairs, getAllPairs }, dispatch)
    }
}

const selector = formValueSelector('manageTournament');

export default reduxForm({
    form: 'manageTournament',
    //enableReinitialize: true
})(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(ManageTournamentForm)
);