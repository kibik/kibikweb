import React, { Component } from 'react';
import {
    Form, 
    Button
} from 'react-bootstrap';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Field, FieldArray, reduxForm } from 'redux-form';
import SimpleInputFormField from '../../commonComponents/formComponents/SimpleInputFormField';
import SimpleTextareaFormField from '../../commonComponents/formComponents/SimpleTextareaFormField';
import SimpleRadioButtonFormField from '../../commonComponents/formComponents/SimpleRadioButtonFormField';
import SimpleDropdownFormField from '../../commonComponents/formComponents/SimpleDropdownFormField';
import SimpleRangeDateTimePickerFormField from '../../commonComponents/formComponents/SimpleRangeDateTimePickerFormField';
import SimpleCheckboxFormField from '../../commonComponents/formComponents/SimpleCheckboxFormField';
import { getAllLocations } from '../../../redux/actions';

const RadioButtonGroup = styled.div`
    display: block;
    padding: 5px;
    margin: 1px;
`

class TournamentForm extends Component {
    onFormSubmit = formValues => {
        this.props.onSubmit(formValues);
    }

    getLocations = () => {
        if(this.props.locations){
            return this.props.locations;
        }

        return [];
    }

    getMovementMethods = () => {
        return [ {id:1, name: "Howell"}];
    }

    render() {
        return(
            <Form onSubmit={this.props.handleSubmit(this.onFormSubmit)}>
                <Field
                pristine
                    name="Name"
                    type="text"
                    component={SimpleInputFormField}
                    label="Name"
                    placeholder="Enter name"
                />

                <Field
                    name="LocationId"
                    type="text"
                    component={SimpleDropdownFormField}
                    label="Location"
                    placeholder="Enter location"
                    data={this.getLocations()}
                />

                <FieldArray
                    name="dates"
                    component={SimpleRangeDateTimePickerFormField}
                /> 

                <RadioButtonGroup>
                    <Field 
                        name="Type" 
                        component={SimpleRadioButtonFormField} 
                        type="radio" 
                        value="0"
                        label="Double"
                    />

                    <Field 
                        name="Type" 
                        component={SimpleRadioButtonFormField} 
                        type="radio" 
                        value="1"
                        label="Team"
                    />
                </RadioButtonGroup>

                <RadioButtonGroup>
                    <Field 
                        name="IsOpen" 
                        id="isOpen"
                        component={SimpleCheckboxFormField} 
                        type="checkbox" 
                        label="Open tournament for enrollment"
                    />
                </RadioButtonGroup>


                <Field
                    name="Description"
                    //type="textarea"
                    component={SimpleTextareaFormField}
                    label="Description"
                    placeholder="Enter description"
                />

                <Field
                    name="MovementMethod"
                    type="text"
                    component={SimpleDropdownFormField}
                    label="Movement Type"
                    placeholder="Enter movement type"
                    data={this.getMovementMethods()}
                />

                <Field
                    name="Tables"
                    type="number"
                    component={SimpleInputFormField}
                    label="Tables"
                    placeholder="Enter number of tables"
                />

                <Field
                    name="Rounds"
                    type="number"
                    component={SimpleInputFormField}
                    label="Rounds"
                    placeholder="Enter number of rounds"
                />

                <Field
                    name="Boards"
                    type="number"
                    component={SimpleInputFormField}
                    label="Boards"
                    placeholder="Enter number of boards"
                />

                <Button variant="primary" type="submit">
                    {this.props.buttonName}
                </Button>
            </Form>
        );
    }   
}

const validate = formValues => {
    const errors = {};
  
    if (!formValues.Name) {
      errors.Name = 'You must enter a title';
    }
  
    if (!formValues.Type) {
      errors.Type = 'You must specify the type of the tournament';
    }
  
    return errors;
}

const mapStateToProps = state => {
    return { 
        locations: state.locations.locations
    };
}

  
export default reduxForm({
form: 'tournamentForm',
validate,
enableReinitialize: true
})(
    connect(mapStateToProps, {getAllLocations})(TournamentForm)
);