import React, { Component } from 'react';
import {
    Form, 
    Button
} from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import SimpleInputFormField from '../../commonComponents/formComponents/SimpleInputFormField';
import SimpleDropdownFormField from '../../commonComponents/formComponents/SimpleDropdownFormField';
import SimpleDateTimePickerFormField from '../../commonComponents/formComponents/SimpleDateTimePickerFormField';
import { getAllLocations } from '../../../redux/actions';

class ClubForm extends Component {
    componentDidMount(){
        this.props.getAllLocations();
    }

    onFormSubmit = formValues => {
        this.props.onSubmit(formValues);
    }

    getLocations = () => {
        if(this.props.locations){
            return this.props.locations;
        }

        return [];
    }

    render() {
        return(
            <Form onSubmit={this.props.handleSubmit(this.onFormSubmit)}>
                <Field
                    name="Name"
                    type="text"
                    component={SimpleInputFormField}
                    label="Name"
                    placeholder="Enter name"
                />

                <Field
                    name="LocationId"
                    type="text"
                    component={SimpleDropdownFormField}
                    label="Location"
                    placeholder="Enter location"
                    data={this.getLocations()}
                />

                <Field
                    name="DateEstablished"
                    component={SimpleDateTimePickerFormField}
                    label="Established at"
                />
    
                <Button variant="primary" type="submit">
                    {this.props.buttonName}
                </Button>
            </Form>
        );
    }
}

const validate = formValues => {
    const errors = {};
  
    if (!formValues.Name) {
      errors.Name = 'You must enter a name';
    }
  
    if (!formValues.LocationId) {
        errors.LocationId = 'You must choose a location';
    }

    return errors;
}
  
const mapStateToProps = state => {
    return { 
        locations: state.locations.locations
    };
}

export default reduxForm({
    form: 'clubForm',
    validate,
    enableReinitialize: true
})(
    connect(mapStateToProps, {getAllLocations})(ClubForm)
);