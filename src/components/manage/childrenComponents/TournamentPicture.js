import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import styled from 'styled-components'
import { Modal, Button } from 'react-bootstrap';
import tournamentDefault from '../../../images/tournament-default.jpg';
import { uploadTournamentPicture, getTournamentPicture } from '../../../redux/actions';

const InvisibleButton = styled.button`
    background-color: Transparent;
    background-repeat:no-repeat;
    border: none;
    cursor:pointer;
    overflow: hidden;
    outline:none;
`;

const ImageContainer = styled.img`
    max-width:100%;
    max-height:100%;
`;

class TournamentPicture extends React.Component{
    constructor(props, context) {
        super(props, context);
    
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleUpload = this.handleUpload.bind(this);

        this.state = {
          show: false,
          file: null,
          fileData: null
        };
    }

    // componentDidMount(){
    //     const { tournamentId } = this.props; 
    //     this.props.getTournamentPicture(tournamentId);
    // }

    handleChange(event) {
        this.setState({
          file: URL.createObjectURL(event.target.files[0]),
          fileData: event.target.files[0]
        })
    }

    handleClose() {
        this.setState({ show: false, file: null, fileData: null });
    }
    
    handleShow() {
        this.setState({ show: true });
    }

    handleUpload() {
        this.props.uploadTournamentPicture(this.props.tournamentId, this.state.fileData);

        this.setState({ show: false, file: null, fileData: null });
    }

    renderPicture = () => {
        if(this.props.tournamentPicture){
            return this.props.tournamentPicture;
        }

        return tournamentDefault;
    }

    render() {
        return(
            <React.Fragment>
                <InvisibleButton onClick={this.handleShow}>
                    <img className="rounded" alt="Tournament profile" src={ this.renderPicture() } />
                </InvisibleButton>

                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Change tournament picture</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div>
                            <input type="file" onChange={this.handleChange}/>
                            <ImageContainer src={this.state.file} />
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.handleClose}>
                            Close
                        </Button>
                        <Button variant="primary" onClick={this.handleUpload}>
                            Upload
                        </Button>
                    </Modal.Footer>
                </Modal>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    if(!state.tournaments.tournament){
        return{}
    }
    console.log(state.tournaments.tournament)
    return { 
        tournamentPicture: state.tournaments.tournament.Logo,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        ...bindActionCreators({ uploadTournamentPicture, getTournamentPicture }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TournamentPicture);