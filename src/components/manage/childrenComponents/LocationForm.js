import React, { Component } from 'react';
import {
    Form, 
    Button
} from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import SimpleInputFormField from '../../commonComponents/formComponents/SimpleInputFormField';

class LocationForm extends Component {
    onFormSubmit = formValues => {
        this.props.onSubmit(formValues);
    }

    render() {
        return(
            <Form onSubmit={this.props.handleSubmit(this.onFormSubmit)}>
                <Field
                        name="Name"
                        type="text"
                        component={SimpleInputFormField}
                        label="Name"
                        placeholder="Enter name"
                    />

                    <Field
                        name="Address"
                        type="text"
                        component={SimpleInputFormField}
                        label="Address"
                        placeholder="Enter address"
                    />

                    <Field
                        name="City"
                        type="text"
                        component={SimpleInputFormField}
                        label="City"
                        placeholder="Enter city name"
                    />

                    <Field
                        name="Country"
                        type="text"
                        component={SimpleInputFormField}
                        label="Country"
                        placeholder="Enter country name"
                    />

                    <Field
                        name="PostCode"
                        type="text"
                        component={SimpleInputFormField}
                        label="Post Code"
                        placeholder="Enter post code"
                    />
                    <Field
                        name="TablesCount"
                        type="number"
                        component={SimpleInputFormField}
                        label="TablesCount"
                        placeholder="Enter tables count"
                    />
                    
                <Button variant="primary" type="submit">
                    {this.props.buttonName}
                </Button>
            </Form>
        );
    }
}

const validate = formValues => {
    const errors = {};
    
    if (!formValues.Name) {
      errors.Name = 'You must enter a name';
    } 

    // if (!formValues.Address) {
    //     errors.Address = 'You must enter an address';
    // } 
  
    // if (!formValues.City) {
    //     errors.City = 'You must enter a name';
    // } 

    return errors;
}
  
export default reduxForm({
    form: 'locationForm',
    validate
})(LocationForm);