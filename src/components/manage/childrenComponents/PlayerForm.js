import React, { Component } from 'react';
import {
    Form, 
    Button
} from 'react-bootstrap';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import SimpleInputFormField from '../../commonComponents/formComponents/SimpleInputFormField';
import SimpleDropdownFormField from '../../commonComponents/formComponents/SimpleDropdownFormField';
import SimpleDateTimePickerFormField from '../../commonComponents/formComponents/SimpleDateTimePickerFormField';
import { getAllClubs } from '../../../redux/actions';

class PlayerForm extends Component {
    componentDidMount(){
        this.props.getAllClubs();
    }

    onFormSubmit = formValues => {
        this.props.onSubmit(formValues);
    }

    getClubs = () => {
        if(this.props.clubs){
            return this.props.clubs;
        }

        return [];
    }

    render() {
        return(
            <Form onSubmit={this.props.handleSubmit(this.onFormSubmit)}>
                <Field
                        name="firstName"
                        type="text"
                        component={SimpleInputFormField}
                        label="First Name"
                        placeholder="Enter first name"
                    />

                    <Field
                        name="middleName"
                        type="text"
                        component={SimpleInputFormField}
                        label="Middle Name"
                        placeholder="Enter middle name"
                    />

                    <Field
                        name="lastName"
                        type="text"
                        component={SimpleInputFormField}
                        label="Last Name"
                        placeholder="Enter last name"
                    />

                    <Field
                        name="email"
                        type="email"
                        component={SimpleInputFormField}
                        label="Email"
                        placeholder="Enter email"
                    />

                    <Field
                        name="birthDate"
                        component={SimpleDateTimePickerFormField}
                        label="Date of birth"
                    />

                    <Field
                        name="country"
                        type="text"
                        component={SimpleInputFormField}
                        label="Country"
                        placeholder="Enter country name"
                    />

                    <Field
                        name="ibn"
                        type="text"
                        component={SimpleInputFormField}
                        label="IBN"
                        placeholder="Enter IBN number"
                    />

                    <Field
                        name="clubId"
                        type="text"
                        component={SimpleDropdownFormField}
                        label="Club"
                        placeholder="Enter club"
                        data={this.getClubs()}
                    />
                    
                <Button variant="primary" type="submit">
                    {this.props.buttonName}
                </Button>
            </Form>
        );
    }
}

const validate = formValues => {
    const errors = {};
    
    if (!formValues.firstName) {
      errors.firstName = 'You must enter first name';
    } 

    if (!formValues.lastName) {
        errors.lastName = 'You must enter last name';
    }

    if (!formValues.email) {
        errors.email = 'You must enter an email';
    } 

    return errors;
}
  
const mapStateToProps = state => {
    return { 
        clubs: state.clubs.clubs
    };
}


export default reduxForm({
    form: 'playerForm',
    validate
})(
    connect(mapStateToProps, {getAllClubs})(PlayerForm)
);