import React from 'react'
import {connect} from 'react-redux';
import { submitLocationForm } from '../../redux/actions';
import LocationForm from './childrenComponents/LocationForm';

class CreateLocationForm extends React.Component {
    onFormSubmit = (formValues) => {
        this.props.submitLocationForm(formValues);
    }

    render(){
        return(
            <React.Fragment>
                <div><h2>Create Location</h2></div>

                <LocationForm onSubmit={this.onFormSubmit} buttonName="Create">      
                </LocationForm>
            </React.Fragment>
        );
    }
}

export default connect(null, {submitLocationForm})(CreateLocationForm)