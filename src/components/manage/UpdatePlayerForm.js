import React from 'react'
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { submitUpdatePlayerForm, getPlayerById } from '../../redux/actions';
import PlayerForm from './childrenComponents/PlayerForm';

class UpdatePlayerForm extends React.Component {
    componentDidMount(){
        const { playerId } = this.props.match.params; 
        this.props.getPlayerById(playerId);
    }

    onFormSubmit = (formValues) => {
        let { playerId } = this.props.match.params; 
        formValues.playerId = playerId;
        this.props.submitUpdatePlayerForm(formValues);
    }

    render(){
        if (!this.props.initialValues.firstName) return null;

        return(
            <React.Fragment>
                <div><h2>Update Player</h2></div>

                <PlayerForm 
                    onSubmit={this.onFormSubmit} 
                    buttonName="Update" 
                    initialValues={this.props.initialValues}>      
                </PlayerForm>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    let initialValues = state.players.player;
    if (!initialValues) { 
        return { 
            initialValues: { birthDate: new Date() } 
        }
    } 

    initialValues.birthDate = new Date(initialValues.birthDate);
    return { 
        initialValues,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        ...bindActionCreators({ getPlayerById, submitUpdatePlayerForm }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdatePlayerForm)