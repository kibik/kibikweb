import React from 'react'
import {connect} from 'react-redux';
import { submitPlayerForm } from '../../redux/actions';
import PlayerForm from './childrenComponents/PlayerForm';

class CreatePlayerForm extends React.Component {    
    onFormSubmit= (formValues) => {
        this.props.submitPlayerForm(formValues);
    }

    render() {
        return(
            <React.Fragment>
                <div>
                    <h2>Create Player</h2>
                </div>
                
                <PlayerForm 
                    onSubmit={this.onFormSubmit} 
                    buttonName="Create"
                    initialValues={this.props.initialValues}
                >
                </PlayerForm>
            </React.Fragment>
        );
    }
}


export default connect(null, { submitPlayerForm })(CreatePlayerForm)