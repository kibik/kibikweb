import React from 'react'
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import { submitUpdateClubForm, getClubById } from '../../redux/actions';
import ClubForm from './childrenComponents/ClubForm';

class UpdateClubForm extends React.Component {
    componentDidMount(){
        const { clubId } = this.props.match.params; 
        this.props.getClubById(clubId);
    }

    onFormSubmit = (formValues) => {
        let { clubId } = this.props.match.params; 
        formValues.clubId = clubId;
        this.props.submitUpdateClubForm(formValues);
    }

    render(){
        if (!this.props.initialValues.Name) return null;

        return(
            <React.Fragment>
                <div><h2>Update Club</h2></div>

                <ClubForm 
                    onSubmit={this.onFormSubmit} 
                    buttonName="Update" 
                    initialValues={this.props.initialValues}>      
                </ClubForm>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    let initialValues = state.clubs.club;
    if (!initialValues) { 
        return { 
            initialValues: { DateEstablished: new Date() } 
        }
    } 

    initialValues.DateEstablished = new Date(initialValues.DateEstablished);
    return { 
        initialValues,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        ...bindActionCreators({ getClubById, submitUpdateClubForm }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdateClubForm)