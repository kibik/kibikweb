import React from 'react'
import styled from 'styled-components';
import DataTable from 'react-data-table-component';
import differenceBy from 'lodash/differenceBy';
import moment from 'moment';
import {connect} from 'react-redux';
import { getAllTournaments } from '../../redux/actions';
import { Link } from 'react-router-dom'

const columns = [
  {
    name: 'Name',
    selector: 'Name',
    sortable: true,
  },
  {
    name: 'Start Date',
    selector: 'StartDate',
    sortable: true,
    right: true,
    format: d => moment(d.StartDate).format('ll')
  },
  {
    name: 'End Date',
    selector: 'EndDate',
    sortable: true,
    right: true,
    format: d => moment(d.EndDate).format('ll')
  },
  {
    name: 'Rounds',
    selector: 'Rounds',
    sortable: true,
    right: true,
  },
  {
    cell: (row) => (
      <Link className="btn btn-light" key={"update" + row.id} to={"/manage/tournament/update/" + row.Id} >Update</Link>
    ),
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
  },
  {
    cell: (row) => (
      <Link className="btn btn-info" key={"general" + row.id} to={"/manage/tournament/general/" + row.Id }>Manage</Link>
    ),
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
  }
];

const StyledDataTable = styled.div`
    height: 100%;
    overflow-y: hidden; 
`;

const ExpandedSection = ({ data }) => <pre>{data.Description}</pre>;

class ManageTournaments extends React.Component {
    constructor(props) {
        super(props);

        this.state = { 
            selectedRows: [], 
            toggleCleared: false 
        };
    }

    componentDidMount() {
        this.props.getAllTournaments(false);
    }

    handleChange = state => {
        this.setState({ selectedRows: state.selectedRows });
      };

    handleRowClicked = row => {
        console.log(`${row.Name} was clicked!`);
      };
    
    // deleteAll = () => {
    //     const { selectedRows } = this.state;
    //     const rows = selectedRows.map(r => r.name);
    
    //     if (window.confirm(`Are you sure you want to delete:\r ${rows}?`)) {
    //       this.setState(state => ({
    //         toggleCleared: !state.toggleCleared,
    //         data: differenceBy(state.data, state.selectedRows, 'name'),
    //       }));
    //     }
    //   };

    // deleteOne = row => {
    //     if (window.confirm(`Are you sure you want to delete:\r ${row.name}?`)) {
    //       const { data } = this.state;
    //       const index = data.findIndex(r => r === row);
    
    //       this.setState(state => ({
    //         toggleCleared: !state.toggleCleared,
    //         data: [...state.data.slice(0, index), ...state.data.slice(index + 1)],
    //       }));
    //     }
    //   };
      
    render() {
        const actions = [
            <Link className="btn btn-light" key="add" to="/manage/tournament">Add</Link>,
        ];
        
        // const contextActions = [
        //     <Button key="delete" onClick={this.deleteAll} style={{ color: 'red' }} icon>
        //       delete
        //     </Button>,
        //   ];

        return (
            <StyledDataTable>
                <DataTable
                    title="Manage Tournaments"
                    columns={columns}
                    data={this.props.tournaments}
                    keyField="Id"
                    selectableRows 
                    highlightOnHover
                    defaultSortField="StartDate"
                    actions={actions}
                    //contextActions={contextActions}
                    onTableUpdate={this.handleChange}
                    clearSelectedRows={this.state.toggledClearRows}
                    onRowClicked={this.handleRowClicked}
                    pagination
                    expandableRows
                    expandableRowsComponent={<ExpandedSection/>}
                />
            </StyledDataTable>         
        )
    }
}

const mapStateToProps = state => {
    return { 
        tournaments: state.tournaments.tournaments
    };
}

export default connect(mapStateToProps, { getAllTournaments})(ManageTournaments);