import React from 'react';
import { Container } from 'react-bootstrap';
import withAppInsights from '../../src/appInsights';

class Home extends React.Component {
    render(){
        return (
            <Container>
                {/* <div>Hi, from Kibik! Enjoy!</div> */}
            </Container>
        );
    }
}

export default withAppInsights(Home);