import React from 'react'
import {
    Container,
    Button,
    Form
} from 'react-bootstrap'
import { Field, reduxForm } from 'redux-form'
import SimpleInputFormField from '../../components/commonComponents/formComponents/SimpleInputFormField';

class Login extends React.Component {
    onFormSubmit(formValues){
        console.log(formValues)
    }

    render() {
        return(
            <Container>
                <div><h2>Login</h2></div>
                <br></br>
                <Form onSubmit={this.props.handleSubmit(this.onFormSubmit)}>
                    <Field
                        name="email"
                        type="email"
                        component={SimpleInputFormField}
                        label="Email address"
                        placeholder="Enter email"
                    />

                    <Field
                        name="password"
                        type="password"
                        component={SimpleInputFormField}
                        label="Password"
                        placeholder="Enter password"
                    />
    
                    <Button variant="primary" type="submit">
                        Login
                    </Button>
                </Form>
            </Container>
        );
    }
}

const validate =formValues => {
    const errors = {};

    if(!formValues.email){
        errors.email = "Enter email";
    } 

    if(!formValues.password){
        errors.password = "Enter password";
    }

    return errors;
};

export default reduxForm({
    form: 'login',
    validate
  })(Login);