import React from 'react'

export default function NotFound({ location }) {
    return (
      <div>
        <h2>Error 404</h2>
        <h6>
          No match for <code>{location.pathname}</code>
        </h6>
      </div>
    );
  }