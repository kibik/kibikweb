import React from 'react'

function SimpleCheckboxFormField({input, type, meta, placeholder, label }){
    return(
        <div className="form-check form-check-inline">
            <input {...input} className="form-check-input" type={type} name={input.name} id={input.name}/>
            <label className="form-check-label" htmlFor={input.name}>{label}</label>
        </div>
    );
}

export default SimpleCheckboxFormField;