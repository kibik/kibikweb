import React, { Component }from 'react'
import {
    Form,
    Col
} from 'react-bootstrap';
import DatePicker, { registerLocale }  from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import "../../../css/date-time-picker.css";
import enGB from 'date-fns/locale/en-GB';
registerLocale('en-GB', enGB);

class SimpleDateTimePickerFormField extends Component {
    constructor(props){
        super(props);

        let date = props.input.value;
        if(!date){
            date = new Date()
        }

        this.state = {
            date 
        };
    }

    handleChange = (date) => {
        this.setState({
            date
        });
    }

    render(){
        let {input, type, meta, placeholder, label } = this.props;

        return( 
            <Form.Group controlId={input.name}>
                <Form.Label>{label}</Form.Label>
                <DatePicker
                {...input}
                className="form-control"
                selected={this.state.date}
                onChange={this.handleChange}
                popperClassName="dtp-popper"
                locale="en-GB"
                value={this.state.date}
                autoComplete='off'
                />
            </Form.Group>
        );
    }
}

export default SimpleDateTimePickerFormField;