import React from 'react'
import {
    Form
} from 'react-bootstrap'

function SimpleInputFormField({input, type, meta: { touched, error }, placeholder, label }){
    return(
        <Form.Group controlId={input.name}>
            <Form.Label>{label}</Form.Label>
            <Form.Control type={type} placeholder={placeholder} {...input}></Form.Control>
            {touched &&
                ((error && <span>{error}</span>))}
        </Form.Group>
    );
}

export default SimpleInputFormField;