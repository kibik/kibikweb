import React from 'react'
import {
    Form
} from 'react-bootstrap'
import DatePicker, { registerLocale }  from "react-datepicker";
import isAfter from "date-fns/isAfter";
import "react-datepicker/dist/react-datepicker.css";
import enGB from 'date-fns/locale/en-GB';
import { Field } from 'redux-form';
registerLocale('en-GB', enGB);


class SimpleRangeDateTimePickerFormField extends React.Component {
    constructor(props){
        super(props);

        let { fields, meta: { error, submitFailed } } = this.props;

        let {StartDate, EndDate} = fields.get(0);

        this.state = {
            StartDate, EndDate, fields
        };
    }
    
    handleChangeDatePicker = ({ StartDate, EndDate }) => {
        StartDate = StartDate || this.state.StartDate;
        EndDate = EndDate || this.state.EndDate;
    
        if (isAfter(StartDate, EndDate)) {
          EndDate = StartDate;
        }
    
        this.state.fields.get(0).StartDate = StartDate;
        this.state.fields.get(0).EndDate = EndDate;
        this.setState({ StartDate, EndDate });
    };
    
    handleChangeStart = StartDate => this.handleChangeDatePicker({ StartDate });

    handleChangeEnd = EndDate => this.handleChangeDatePicker({ EndDate });
    
    renderStartDateTimePicker =  ({input, type, meta, placeholder, label}) => {     
        return( 
            <Form.Group controlId={input.name}>
                 <Form.Label>{label}</Form.Label>
                 <DatePicker
                    {...input}
                    className="form-control"
                    selected={this.state.StartDate}
                    selectsStart
                    startDate={this.state.StartDate}
                    endDate={this.state.EndDate}
                    onChange={this.handleChangeStart}
                    value={this.state.StartDate}
                    popperClassName="dtp-popper"
                    locale="en-GB"
                />
            </Form.Group>
        );
    }

    renderEndDateTimePicker =  ({input, type, meta, placeholder, label}) => {
        return( 
            <Form.Group controlId={input.name}>
                 <Form.Label>{label}</Form.Label>
                 <DatePicker
                    {...input}
                     className="form-control"
                     selected={this.state.EndDate}
                     selectsEnd
                     startDate={this.state.StartDate}
                     endDate={this.state.EndDate}
                     onChange={this.handleChangeEnd}
                     minDate={this.state.StartDate}
                     value={this.state.EndDate}
                     popperClassName="dtp-popper"
                     locale="en-GB"
                />
            </Form.Group>
        );
    }

    render() {
        return(
            <React.Fragment>
                <Field
                    name="StartDate"
                    //type="text"
                    //component="input"
                    component={this.renderStartDateTimePicker}
                    label="Start date"
                    value={this.state.StartDate}
                />

                <Field
                    name="EndDate"
                    //type="text"
                    //component="input"
                    component={this.renderEndDateTimePicker}
                    label="End date"
                    value={this.state.EndDate}
                />
            </React.Fragment>
        );
    }
}

export default SimpleRangeDateTimePickerFormField;