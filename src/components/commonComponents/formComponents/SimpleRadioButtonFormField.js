import React from 'react'

function SimpleRadioButtonFormField({input, type, meta, placeholder, label }){
    return(
        <div className="form-check form-check-inline">
            <input {...input} className="form-check-input" type={type} name={input.name} id={input.value} value={input.value}/>
            <label className="form-check-label" htmlFor={input.value}>{label}</label>
        </div>
    );
}

export default SimpleRadioButtonFormField;