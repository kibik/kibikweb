import React from 'react'
import {
    Form
} from 'react-bootstrap';

function SimpleDropdownFormField({input, data, type, meta: { touched, error }, placeholder, label }){
    return (
        <Form.Group md="8" controlId={input.name}>
            <Form.Label>{label}</Form.Label>
            <Form.Control as="select" {...input}>
             <option value="">{placeholder}</option>
                {
                    data.map(d => {
                        return ( <option value={d.id} key={d.id}>{d.name}</option>);
                    })
                }
            </Form.Control>
            {touched &&
                ((error && <span>{error}</span>))}
        </Form.Group>
    );
}

export default SimpleDropdownFormField;