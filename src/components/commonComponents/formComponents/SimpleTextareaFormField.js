import React from 'react'
import {
    Form
} from 'react-bootstrap'

function SimpleTextareaFormField({input, type, meta, placeholder, label }){
    return(
        <Form.Group controlId={input.name}>
            <Form.Label>{label}</Form.Label>
            <Form.Control as="textarea" type={type} placeholder={placeholder} {...input}/>
        </Form.Group>
    );
}

export default SimpleTextareaFormField;