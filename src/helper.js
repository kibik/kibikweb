const CardWidth = 225;
const CardHeight = 315;

export const GetCardDimensions = (sign, color) => {
    const getWidth = (sign) => {
        switch(sign){
            case "A":
                return 0;
            case "2":
                return -1 * CardWidth;
            case "3":
                return -2 * CardWidth;
            case "4":
                return -3 * CardWidth;
            case "5":
                return -4 * CardWidth;
            case "6":
                return -5 * CardWidth;
            case "7":
                return -6 * CardWidth;
            case "8":
                return -7 * CardWidth;
            case "9":
                return -8 * CardWidth;
            case "10":
                return -9 * CardWidth;
            case "J":
                return -10 * CardWidth;
            case "Q":
                return -11 * CardWidth;
            case "K":
                return -12 * CardWidth;
            default:
                throw new Error(`Not supported card sign. The provided values are ${sign} & ${color}`);
        }
    }

    let width = 0;
    let height = 0;
    switch(color) {
        case "He":
            width = getWidth(sign);
            return { width, height };
        case "Sp":
            height = -1 * CardHeight;
            width = getWidth(sign);
            return { width, height };
        case "Di":
            height = -2 * CardHeight;
            width = getWidth(sign);
            return { width, height };
        case "Cl":
            height = -3 * CardHeight;
            width = getWidth(sign);
            return { width, height };
        default:
            throw new Error(`Not supported card color. The provided values are ${sign} & ${color}`);
    }
}

export const GetSideRotation = (side) => {
    switch(side){
        case "N":
            return 0;
        case "E":
            return 90;
        case "S":
            return 0;
        case "W":
            return -90;
        default:
            throw new Error(`Not supported side. The provided value is ${side}`);
    }
}