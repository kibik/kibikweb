import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import { Provider } from 'react-redux';
import redux from './redux';

import App from './components/App';

ReactDOM.render(  
    <Provider store={redux}>
        <App />
    </Provider>, 
    document.getElementById('root')
);
