import { ApplicationInsights } from '@microsoft/applicationinsights-web';
import { ReactPlugin, withAITracking } from '@microsoft/applicationinsights-react-js';
import history from '../src/history';

var reactPlugin = new ReactPlugin();
var ai = new ApplicationInsights({
    config: {
        instrumentationKey: '230a8290-d4b3-4016-81fc-4cffaf69a150',
        extensions: [reactPlugin],
        extensionConfig: {
          [reactPlugin.identifier]: { history: history }
        }
    }
});
ai.loadAppInsights();

export default (Component) => withAITracking(reactPlugin, Component)
export const appInsights = ai.appInsights;