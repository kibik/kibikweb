import {
    put, all, fork, takeLatest
} from 'redux-saga/effects';

import {
    GET_ALL_CLUBS,
    GET_CLUB_BY_ID
} from  '../actions/types'

import {
    GET_ALL_CLUBS_SUCCESS,
    GET_CLUB_BY_ID_SUCCESS
} from  '../reducers/types'

function* fetchAllClubs() {
  const json = yield fetch('http://localhost:7071/api/manage/club')
    .then(response => response.json());

  yield put({ type: GET_ALL_CLUBS_SUCCESS, clubs: json, });
}

function* getAllClubs() {
    yield takeLatest(GET_ALL_CLUBS, fetchAllClubs)
}

function* fetchClubById(data) {
  const json = yield fetch('http://localhost:7071/api/manage/club/' + data.clubId)
    .then(response => response.json());
    
  yield put({ type: GET_CLUB_BY_ID_SUCCESS, club: json, });
}

function* getClubById() {
  yield takeLatest(GET_CLUB_BY_ID, fetchClubById)
}

export default function* rootSaga() {
    yield all([
        fork(getAllClubs),
        fork(getClubById)
    ]);
  }