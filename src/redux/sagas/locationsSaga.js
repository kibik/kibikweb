import {
    put, all, fork, takeLatest
} from 'redux-saga/effects';

import {
    GET_ALL_LOCATIONS,
    GET_LOCATION_BY_ID
} from  '../actions/types'

import {
    GET_ALL_LOCATIONS_SUCCESS,
    GET_LOCATION_BY_ID_SUCCESS
} from  '../reducers/types'

function* fetchAllLocations() {
  const json = yield fetch('http://localhost:7071/api/manage/location')
    .then(response => response.json());

  yield put({ type: GET_ALL_LOCATIONS_SUCCESS, locations: json, });
}

function* getAllLocations() {
    yield takeLatest(GET_ALL_LOCATIONS, fetchAllLocations)
}

function* fetchLocationById(data) {
  const json = yield fetch('http://localhost:7071/api/manage/location/' + data.locationId)
    .then(response => response.json());
    
  yield put({ type: GET_LOCATION_BY_ID_SUCCESS, location: json, });
}

function* getLocationById() {
  yield takeLatest(GET_LOCATION_BY_ID, fetchLocationById)
}

export default function* rootSaga() {
    yield all([
        fork(getAllLocations),
        fork(getLocationById)
    ]);
  }