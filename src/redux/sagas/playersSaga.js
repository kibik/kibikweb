import {
    put, all, fork, takeLatest
} from 'redux-saga/effects';

import {
    GET_ALL_PLAYERS,
    GET_PLAYER_BY_ID
} from  '../actions/types'

import {
    GET_ALL_PLAYERS_SUCCESS,
    GET_PLAYER_BY_ID_SUCCESS
} from  '../reducers/types'

function* fetchAllPlayers() {
  const json = yield fetch('http://localhost:7071/api/player')
    .then(response => response.json());

  yield put({ type: GET_ALL_PLAYERS_SUCCESS, players: json, });
}

function* getAllPlayers() {
    yield takeLatest(GET_ALL_PLAYERS, fetchAllPlayers)
}

function* fetchPlayerById(data) {
  const json = yield fetch('http://localhost:7071/api/player/' + data.playerId)
    .then(response => response.json());

  yield put({ type: GET_PLAYER_BY_ID_SUCCESS, player: json, });
}

function* getPlayerById() {
  yield takeLatest(GET_PLAYER_BY_ID, fetchPlayerById)
}

export default function* rootSaga() {
    yield all([
        fork(getAllPlayers),
        fork(getPlayerById)
    ]);
  }