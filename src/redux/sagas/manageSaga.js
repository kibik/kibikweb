import {
    put, all, takeEvery, fork, call, take, takeLatest
} from 'redux-saga/effects';
import {startSubmit, stopSubmit } from 'redux-form';
import axios from 'axios';
import history from '../../history';

import {
  SUBMIT_TOURNAMENT_FORM,
  SUBMIT_UPDATE_TOURNAMENT_FORM,
  SUBMIT_LOCATION_FORM,
  SUBMIT_UPDATE_LOCATION_FORM,
  SUBMIT_CLUB_FORM,
  SUBMIT_UPDATE_CLUB_FORM,
  SUBMIT_PLAYER_FORM,
  SUBMIT_UPDATE_PLAYER_FORM,
  UPDATE_TOURNAMENT_PICTURE,
  DELETE_LOCATIONS,
  GET_ALL_PAIRS,
  GET_TOURNAMENT_PAIRS
} from '../actions/types'

import {
  CREATE_TOURNAMENT_SUCCESS,
  GET_TOURNAMENT_PICTURE_SUCCESS,
  DELETE_LOCATIONS_SUCCESS,
  GET_ALL_PAIRS_SUCCESS,
  GET_TOURNAMENT_PAIRS_SUCCESS
} from '../reducers/types'

// --- TOURNAMENT ---
async function submitTournamentFormToServer(data) {
  return await axios.post('http://localhost:7071/api/manage/tournament', data)
    .then(response => response)
    .catch(error => console.log(error));
}

function* callSubmitTournamentForm(action) {
  yield put(startSubmit("tournamentForm"));
  let errors = {};
  const result = yield call(submitTournamentFormToServer, action.payload);

  if(result && result.errors){
    yield put({type: 'REQUEST_FAILED', errors: result.errors});
    errors = result.errors;
  } else {  
    yield put({type: CREATE_TOURNAMENT_SUCCESS, tournament: action.payload});
    history.push("/manage/tournaments");
  }

  yield put(stopSubmit('tournamentForm', errors));
}

function* submitTournamentForm() {
  yield takeEvery(SUBMIT_TOURNAMENT_FORM, callSubmitTournamentForm);
}

async function submitUpdateTournamentFormToServer(data) {
  return await axios.put('http://localhost:7071/api/manage/tournament', data)
    .then(response => response)
    .catch(error => console.log(error));
}

function* callSubmitUpdateTournamentForm(action) {
  yield put(startSubmit("tournamentForm"));
  let errors = {};
  const result = yield call(submitUpdateTournamentFormToServer, action.payload);

  if(result && result.errors){
    yield put({type: 'REQUEST_FAILED', errors: result.errors});
    errors = result.errors;
  } else {
    yield put({type: 'REQUEST_SUCCESSFUL'});
    history.push("/manage/tournaments");
    //redirect to another page
  }

  yield put(stopSubmit('tournamentForm', errors));
}

function* submitUpdateTournamentForm() {
  yield takeEvery(SUBMIT_UPDATE_TOURNAMENT_FORM, callSubmitUpdateTournamentForm);
}

// --- LOCATION ---
async function submitLocationFormToServer(data) {
  return await axios.post('http://localhost:7071/api/manage/location', data)
    .then(response => response)
    .catch(error => console.log(error));
}

function* callSubmitLocationForm(action) {
  yield put(startSubmit("locationForm"));
  let errors = {};
  const result = yield call(submitLocationFormToServer, action.payload);

  if(result && result.errors){
    yield put({type: 'REQUEST_FAILED', errors: result.errors});
    errors = result.errors;
  } else {
    yield put({type: 'REQUEST_SUCCESSFUL'});
    history.push("/manage/locations");
  }

  yield put(stopSubmit('locationForm', errors));
}

function* submitLocationForm() {
  yield takeEvery(SUBMIT_LOCATION_FORM, callSubmitLocationForm);
}

async function submitUpdateLocationFormToServer(data) {
  return await axios.put('http://localhost:7071/api/manage/location', data)
    .then(response => response)
    .catch(error => console.log(error));
}

function* callSubmitUpdateLocationForm(action) {
  yield put(startSubmit("locationForm"));
  let errors = {};
  const result = yield call(submitUpdateLocationFormToServer, action.payload);

  if(result && result.errors){
    yield put({type: 'REQUEST_FAILED', errors: result.errors});
    errors = result.errors;
  } else {
    yield put({type: 'REQUEST_SUCCESSFUL'});
    history.push("/manage/locations");
  }

  yield put(stopSubmit('locationForm', errors));
}

function* submitUpdateLocationForm() {
  yield takeEvery(SUBMIT_UPDATE_LOCATION_FORM, callSubmitUpdateLocationForm);
}

async function deleteLocationsFromServer(data) {
  return await axios.delete("http://localhost:7071/api/manage/location", {data})
  .then(response => response)
  .catch(error => console.log(error));
}

function* callDeleteLocations(action){
  const result = yield call(deleteLocationsFromServer, action.locations);
  if(!result.errors){
    yield put({type: DELETE_LOCATIONS_SUCCESS, deleteLocations: action.locations});
  }
}

function* deleteLocations(){
  yield takeEvery(DELETE_LOCATIONS, callDeleteLocations);
}

// --- CLUB ---
async function submitClubFormToServer(data) {
  return await axios.post('http://localhost:7071/api/manage/club', data)
    .then(response => response)
    .catch(error => console.log(error));
}

function* callSubmitClubForm(action) {
  yield put(startSubmit("clubForm"));
  let errors = {};
  const result = yield call(submitClubFormToServer, action.payload);

  if(result && result.errors){
    yield put({type: 'REQUEST_FAILED', errors: result.errors});
    errors = result.errors;
  } else {
    yield put({type: 'REQUEST_SUCCESSFUL'});
    history.push("/manage/clubs");
  }

  yield put(stopSubmit('clubForm', errors));
}

function* submitClubForm() {
  yield takeEvery(SUBMIT_CLUB_FORM, callSubmitClubForm);
}

async function submitUpdateClubormToServer(data) {
  return await axios.put('http://localhost:7071/api/manage/club', data)
    .then(response => response)
    .catch(error => console.log(error));
}

function* callSubmitUpdateClubForm(action) {
  yield put(startSubmit("clubForm"));
  let errors = {};
  const result = yield call(submitUpdateClubormToServer, action.payload);

  if(result && result.errors){
    yield put({type: 'REQUEST_FAILED', errors: result.errors});
    errors = result.errors;
  } else {
    yield put({type: 'REQUEST_SUCCESSFUL'});
    history.push("/manage/clubs");
  }

  yield put(stopSubmit('clubForm', errors));
}

function* submitUpdateClubForm() {
  yield takeEvery(SUBMIT_UPDATE_CLUB_FORM, callSubmitUpdateClubForm);
}

async function uploadPicture(payload) {
  const data = new FormData();
  data.append('File', payload.file);

  let config =  {
    headers : {
      'Content-Type': `multipart/form-data; boundary=${data._boundary}`
    }
  };

  return await axios.post(`http://localhost:7071/api/manage/tournament/${payload.tournamentId}/image`, payload.file, config)
                    .then(response => response.data)
                    .catch(error => console.log(error));
}

function* callUploadSource(action) {
  const result = yield call(uploadPicture, action.payload);

  if(result && result.errors){
    yield put({type: 'REQUEST_FAILED', errors: result.errors});
  } else {
    yield put({type: GET_TOURNAMENT_PICTURE_SUCCESS, tournamentPicture: result});
  }
}

function* uploadTournamentPicture() {
  yield takeEvery(UPDATE_TOURNAMENT_PICTURE, callUploadSource);
}

// --- PLAYER ---

async function submitPlayerFormToServer(data) {
  return await axios.post('http://localhost:7071/api/player', data)
    .then(response => response)
    .catch(error => console.log(error));
}

function* callSubmitPlayerForm(action) {
  yield put(startSubmit("playerForm"));
  let errors = {};
  const result = yield call(submitPlayerFormToServer, action.payload);

  if(result && result.errors){
    yield put({type: 'REQUEST_FAILED', errors: result.errors});
    errors = result.errors;
  } else {
    yield put({type: 'REQUEST_SUCCESSFUL'});
    history.push("/manage/players");
  }

  yield put(stopSubmit('playerForm', errors));
}

function* submitPlayerForm() {
  yield takeEvery(SUBMIT_PLAYER_FORM, callSubmitPlayerForm);
}

async function submitUpdatePlayerFormToServer(data) {
  return await axios.put('http://localhost:7071/api/player', data)
    .then(response => response)
    .catch(error => console.log(error));
}

function* callSubmitUpdatePlayerForm(action) {
  yield put(startSubmit("playerForm"));
  let errors = {};
  const result = yield call(submitUpdatePlayerFormToServer, action.payload);

  if(result && result.errors){
    yield put({type: 'REQUEST_FAILED', errors: result.errors});
    errors = result.errors;
  } else {
    yield put({type: 'REQUEST_SUCCESSFUL'});
    history.push("/manage/players");
  }

  yield put(stopSubmit('playerForm', errors));
}

function* submitUpdatePlayerForm() {
  yield takeEvery(SUBMIT_UPDATE_PLAYER_FORM, callSubmitUpdatePlayerForm);
}

function* fetchAllPairs(data) {
  const json = yield fetch(`http://localhost:7071/api/manage/pair`)
      .then(response => response.json());

  yield put({ type: GET_ALL_PAIRS_SUCCESS, pairs: json });
}

function* getAllPairs() {
  yield takeLatest(GET_ALL_PAIRS, fetchAllPairs)
}

function* fetchTournamentPairs(data) {
  const json = yield fetch(`http://localhost:7071/api/manage/tournament/${data.tournamentId}/pair`)
      .then(response => response.json());

  yield put({ type: GET_TOURNAMENT_PAIRS_SUCCESS, tournamentPairs: json, });
}

function* getTournamentPairs() {
  yield takeLatest(GET_TOURNAMENT_PAIRS, fetchTournamentPairs)
}

export default function* rootSaga() {
    yield all([
      fork(submitTournamentForm),
      fork(submitUpdateTournamentForm),
      fork(submitLocationForm),
      fork(submitUpdateLocationForm),
      fork(submitClubForm),
      fork(submitUpdateClubForm),
      fork(uploadTournamentPicture),
      fork(submitPlayerForm),
      fork(submitUpdatePlayerForm),
      fork(deleteLocations),
      fork(getAllPairs),
      fork(getTournamentPairs),
    ]);
  }