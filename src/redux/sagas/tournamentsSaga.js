import {
    put, all, fork, takeLatest, takeEvery
} from 'redux-saga/effects';

import {
    GET_ALL_TOURNAMENTS,
    GET_TOURNAMENT_BY_ID,
    GET_TOURNAMENT_PICTURE
} from  '../actions/types'

import {
  GET_ALL_TOURNAMENTS_SUCCESS, 
  GET_TOURNAMENT_BY_ID_SUCCESS,
  GET_TOURNAMENT_PICTURE_SUCCESS
} from  '../reducers/types'

function* fetchAllTournaments(data) {
  const json = yield fetch(`http://localhost:7071/api/tournament/${data.requirePicture}`)
    .then(response => response.json());

  yield put({ type: GET_ALL_TOURNAMENTS_SUCCESS, tournaments: json });
}

function* getAllTournaments() {
    yield takeLatest(GET_ALL_TOURNAMENTS, fetchAllTournaments)
}

function* fetchTournamentById(data) {
    console.log(data);
  const json = yield fetch(`http://localhost:7071/api/tournament/${data.tournamentId}/${data.requirePicture}/`)
    .then(response => response.json());
    
  yield put({ type: GET_TOURNAMENT_BY_ID_SUCCESS, tournament: json, });
}

function* getTournamentById() {
  yield takeLatest(GET_TOURNAMENT_BY_ID, fetchTournamentById)
}

function* fetchTournamentPicture(data) {
  const blobLink = yield fetch(`http://localhost:7071/api/manage/tournament/${data.tournamentId}/image`)
    .then(response => response.text());
    
  yield put({ type: GET_TOURNAMENT_PICTURE_SUCCESS, tournamentPicture: blobLink, tournamentId: data.tournamentId });
}

function* getTournamentPicture() {
  yield takeEvery(GET_TOURNAMENT_PICTURE, fetchTournamentPicture)
}


export default function* rootSaga() {
    yield all([
        fork(getAllTournaments),
        fork(getTournamentById),
        fork(getTournamentPicture)
    ]);
  }