import {
    all
} from 'redux-saga/effects';
import gameSaga from './gameSaga';
import manageSaga from './manageSaga';
import tournamentSaga from './tournamentsSaga';
import locationsSaga from './locationsSaga';
import clubsSaga from './clubsSaga';
import playersSaga from './playersSaga';

export default function* rootSaga() {
    yield all([
        gameSaga(),
        manageSaga(),
        tournamentSaga(),
        locationsSaga(),
        clubsSaga(),
        playersSaga()
    ]);
}