import {
    put, all, fork, takeLatest
} from 'redux-saga/effects';

import {
    GET_GAME_RESULTS,
    GET_GAME,
    GET_ALL_GAMES_BY_TOURNAMENT_ID,
    GET_TOURNAMENT_PAIRS
} from '../actions/types'

import {
    GET_GAME_RESULTS_SUCCESS,
    GET_GAME_SUCCESS,
    GET_ALL_GAMES_BY_TOURNAMENT_ID_SUCCESS,
    GET_TOURNAMENT_PAIRS_SUCCESS,
} from  '../reducers/types'

function* fetchLastestPlayedGameByTournamentId(data) {
    const json = yield fetch(`http://localhost:7071/api/results/${data.tournamentId}`)
      .then(response => response.json());
      
    yield put({ type: GET_GAME_RESULTS_SUCCESS, results: json, });
  }
  
function* getLastestPlayedGameByTournamentId() {
  yield takeLatest(GET_GAME_RESULTS, fetchLastestPlayedGameByTournamentId)
}

function* fetchGameById(data) {
  const json = yield fetch(`http://localhost:7071/api/game/${data.gameId}`)
    .then(response => response.json());
    
  yield put({ type: GET_GAME_SUCCESS, game: json, });
}

function* getGameById() {
yield takeLatest(GET_GAME, fetchGameById)
}

function* fetchAllGamesByTournamentId(data) {
  const json = yield fetch(`http://localhost:7071/api/game/tournament/${data.tournamentId}`)
    .then(response => response.json());
    
  yield put({ type: GET_ALL_GAMES_BY_TOURNAMENT_ID_SUCCESS, rounds: json, });
}

function* getAllGamesByTournamentId() {
yield takeLatest(GET_ALL_GAMES_BY_TOURNAMENT_ID, fetchAllGamesByTournamentId)
}

export default function* rootSaga() {
    yield all([
      fork(getLastestPlayedGameByTournamentId),
      fork(getGameById),
      fork(getAllGamesByTournamentId)
    ]);
}