import { 
  // SIGN_IN,
  // SIGN_OUT,
  FETCH_GAMES,
  FETCH_GAME_DETAILS,
  FETCH_GAME,
  FETCH_COMMENTS_BY_GAME,
  SUBMIT_TOURNAMENT_FORM,
  SUBMIT_UPDATE_TOURNAMENT_FORM,
  SUBMIT_LOCATION_FORM,
  SUBMIT_UPDATE_LOCATION_FORM,
  SUBMIT_CLUB_FORM,
  SUBMIT_UPDATE_CLUB_FORM,
  SUBMIT_PLAYER_FORM,
  SUBMIT_UPDATE_PLAYER_FORM,
  GET_ALL_TOURNAMENTS,
  GET_TOURNAMENT_BY_ID,
  GET_ALL_LOCATIONS,
  GET_LOCATION_BY_ID,
  GET_ALL_CLUBS,
  GET_CLUB_BY_ID,
  GET_ALL_PLAYERS,
  GET_PLAYER_BY_ID,
  UPDATE_TOURNAMENT_PICTURE,
  GET_TOURNAMENT_PICTURE,
  DELETE_LOCATIONS,
  GET_GAME_RESULTS,
  GET_ALL_PAIRS,
  GET_ALL_GAMES_BY_TOURNAMENT_ID,
  GET_GAME,
  GET_TOURNAMENT_PAIRS,
} from './types'

const games = [
  {
      id: 1,
      title: "Tournament 1",
      description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vel aliquet magna, feugiat tincidunt lorem. Nulla venenatis pellentesque massa eget cursus. Etiam porttitor ex at arcu auctor, quis tempor erat rutrum. Vivamus tellus massa, consequat non bibendum vitae, faucibus ut dui. Cras bibendum, ante id dignissim volutpat, justo odio finibus dui, vel elementum arcu urna sed urna. Nunc tincidunt dui nec mauris ullamcorper rhoncus. In aliquam tincidunt justo ut pellentesque. Mauris non malesuada turpis. Sed sit amet scelerisque nunc. Curabitur posuere turpis eget purus gravida congue eget eu tortor. Mauris lacinia et lectus non rhoncus.",
      picture: "https://i.ytimg.com/vi/osJbZEvrSXI/maxresdefault.jpg",
  },
  {
      id: 2,
      title: "Tournament 2",
      description: "Praesent quam orci, hendrerit at iaculis fermentum, luctus id sem. Donec enim odio, pulvinar ut ipsum eu, eleifend efficitur elit. Sed posuere augue velit, non viverra sapien scelerisque at. Etiam pellentesque sollicitudin porttitor. Morbi sodales lectus ante, ultrices varius ante molestie quis. Ut imperdiet dapibus orci, nec aliquet nunc elementum a. Quisque vitae magna sed nibh commodo bibendum sed ac velit. Etiam eu blandit turpis. Curabitur in posuere ante. Maecenas sollicitudin eleifend nunc eu auctor. Phasellus aliquet elit nec nulla volutpat laoreet. Etiam nulla enim, pellentesque nec metus id, facilisis consectetur elit. Nunc quis sodales mi. Sed semper nec nunc at iaculis. Cras vel egestas mi.",
      picture: "https://i.ytimg.com/vi/3c2a7i4Y3Xk/maxresdefault.jpg"
  },
  {
      id: 3,
      title: "Tournament 3",
      description: "Nulla facilisi. Curabitur condimentum sapien magna, non auctor mauris rhoncus at. Vestibulum vitae scelerisque neque, at varius tellus. Duis nec cursus nunc. Proin malesuada convallis orci, sit amet feugiat leo vehicula a. Aliquam imperdiet eros eget dictum tristique. Donec ultricies, leo at euismod cursus, ex metus mattis mauris, et dignissim purus lacus ut dolor.",
      picture: "https://i.ytimg.com/vi/2Y6oP7yGj84/maxresdefault.jpg"
  },
  {
      id: 4,
      title: "Tournament 4",
      description: "Nullam in tincidunt risus, ut sodales elit. Proin quis felis quis arcu euismod blandit eget quis elit. Proin velit nibh, laoreet a porttitor ut, iaculis vitae velit. Fusce ac ultricies ipsum. Mauris nec luctus arcu. Duis eleifend fermentum vulputate. Vestibulum aliquam in lacus et vehicula. Vivamus et efficitur neque, ut sagittis ex. In nibh velit, iaculis eget orci sed, cursus condimentum dui. Aenean sodales mi et massa mollis, aliquet euismod enim accumsan.",
      picture: "https://techreport.com/r.x/2015_11_9_Unreal_Tournament_4_is_coming_along_nicely/ut4-face.jpg"
  },
  {
      id: 5,
      title: "Tournament 5",
      description: "Phasellus tincidunt erat eget diam ornare, eget dapibus tellus sagittis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed diam enim, dignissim sit amet nunc et, porttitor vehicula neque. Sed orci magna, lobortis ut massa vitae, tincidunt sodales dui. Suspendisse non est at neque sagittis porta in eu nisi. Aliquam erat volutpat. Duis mi est, pulvinar eget diam efficitur, vehicula aliquam eros. Curabitur tempor quam ut diam mollis pharetra. Nulla dictum sapien ut varius aliquam. Vestibulum eget facilisis odio. In posuere a nulla at varius. Proin auctor ante ipsum, vitae scelerisque arcu imperdiet nec. Vivamus nec euismod urna.",
      picture: "https://www.printyourbrackets.com/thumbs/5-team-tournament-bracket.gif"
  },
];

const comments = [
    {
    id: 1,
    gameId: 1,
    author: "Ivan Ivanov",
    profilePicture: "https://cache1.24chasa.bg/Images/cache/117/Image_4269117_308_0.jpg",
    content: "Hi, I am Ivan! Thank you for inviting me!"
    },
    {
    id: 2,
    gameId: 1,
    author: "Petar Petrov",
    profilePicture: "http://cache1.168chasa.bg/Images/Cache/293/Image_6015293_500_0.jpg",
    content: "Hi, Vanka! Come here and watch!"
    },
    {
    id: 3,
    gameId: 1,
    author: "Georgi Georgiev",
    profilePicture: "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/G._Georgiev_Getsata_2013.jpg/276px-G._Georgiev_Getsata_2013.jpg",
    content: "Ari, she gledame li?"
    },
    {
    id: 4,
    gameId: 1,
    author: "Todor Todorov",
    profilePicture: "https://assess.bg/images/content/team/todor-todorov.jpg",
    content: "Basi impasa..."
    },
    {
      id: 5,
      gameId: 1,
      author: "Ivo Ivanov",
      profilePicture: "https://i2.offnews.bg/events/2016/02/23/624661/php6hdk7a_800x*.jpg",
      content: "Opa, opa, opa... Zdr bebceta :* ko pr?"
    },
    {
      id: 6,
      gameId: 1,
      author: "Ivailo Ivailov",
      profilePicture: "https://pbs.twimg.com/profile_images/562216916811792384/AMPJDpzs.jpeg",
      content: "n6 a ti"
    },
    {
      id: 7,
      gameId: 1,
      author: "Velizar Velizarov",
      profilePicture: "https://www.dunavmost.com/snimka/201509/728_416/0d168a79648f4ad69105128dd8f69bb1.jpg",
      content: "n6 a ti"
    },
    {
      id: 8,
      gameId: 1,
      author: "Penka Penkova",
      profilePicture: "http://focus-radio.net/wp-content/uploads/2018/10/26ee77e4ec428a83f9975b7f4d74835d.jpg",
      content: "i az taka"
    },
    {
      id: 9,
      gameId: 1,
      author: "Ivanka Ivankova",
      profilePicture: "https://pp.userapi.com/c849032/v849032914/c55b4/654rS0SfjgA.jpg?ava=1",
      content: "idi na hui"
    },
    {
      id: 10,
      gameId: 1,
      author: "Gergina Gerginova",
      profilePicture: "http://alumni.academyglow.com/wp-content/uploads/2017/05/GerginaGeorgieva_IzlojbaVazdushniKuli.jpg",
      content: "ne govorete taka..."
    },
    {
      id: 11,
      gameId: 1,
      author: "Stoyanka Stoyanova",
      profilePicture: "https://m.netinfo.bg/media/images/27563/27563576/640-420-obshtinskiiat-sluzhitel-s-najdylyg-stazh-stoianka-stoianova-pocherpi-kolegi-za.jpg",
      content: "pedali"
    },
    {
      id: 12,
      gameId: 1,
      author: "Velichka Velichkova",
      profilePicture: "https://cache2.24chasa.bg/Images/Cache/998/Image_5089998_126.jpg",
      content: "STOP!"
    }
]

const game = [
  // game setup phase
  {
    "game": [
      {
        "command": "setup",
        "cards": [
          { "Sp": ["5", "4"] } ,
          { "He": ["K", "Q", "10"] },
          { "Di": ["K", "5", "4"] },
          { "Cl": ["A", "K", "8", "5", "4"] }
        ],
        "currentSide": "N"
      },
      {
        "command": "setup",
        "cards": [
          { "Sp": ["10", "9", "8", "6"] },
          { "He": ["9", "6"] },
          { "Di": ["J", "10", "7", "3"] },
          { "Cl": ["Q", "J", "2"] }
        ],
        "currentSide": "E"
      },
      {
        "command": "setup",
        "cards": [
          { "Sp": ["7", "3", "2"] },
          { "He": ["A", "J", "7"] },
          { "Di": ["A", "Q", "9", "8", "6", "4"] },
          { "Cl": ["3"] }
        ],
        "currentSide": "S"
      },
      {
        "command": "setup",
        "cards": [
          { "Sp": ["A", "K", "Q", "J"] },
          { "He": ["8", "5", "4", "3", "2"] },
          { "Di": [] },
          { "Cl": ["10", "9", "7", "6"] }
        ],
        "currentSide": "W"
      }
    ],
    "zones": [
      { "N": true },
      { "E": false },
      { "S": true },
      { "W": false }
    ],
    "startSide": "N"
  },
  // game announce phase
  {}
];

export const fetchGames = () => {
  return { type: FETCH_GAMES, payload: games };
};

export const fetchGameDetails = (gameId) => {
  let gameDetails = games.find(g => g.id === parseInt(gameId));
  return { type: FETCH_GAME_DETAILS, payload: gameDetails }
};

export const fetchGame = (gameId) => {
  return { type: FETCH_GAME, payload: game };
};

export const fetchComments = (gameId) => {
  let gameComments = comments.filter(comment => comment.gameId === parseInt(gameId));
  return { type: FETCH_COMMENTS_BY_GAME, payload: gameComments };
}

export const submitTournamentForm = function(data) {
  return {
    type: SUBMIT_TOURNAMENT_FORM,
    payload: data
  };
};

export const submitUpdateTournamentForm = function(data) {
  console.log("ACTION")
  return {
    type: SUBMIT_UPDATE_TOURNAMENT_FORM,
    payload: data
  };
};

export const submitLocationForm = function(data) {
  return {
    type: SUBMIT_LOCATION_FORM,
    payload: data
  };
};

export const submitUpdateLocationForm = function(data) {
  return {
    type: SUBMIT_UPDATE_LOCATION_FORM,
    payload: data
  };
};

export const submitClubForm = function(data) {
  return {
    type: SUBMIT_CLUB_FORM,
    payload: data
  };
};

export const submitUpdateClubForm = function(data) {
  return {
    type: SUBMIT_UPDATE_CLUB_FORM,
    payload: data
  };
};

export const submitPlayerForm = function(data) {
  return {
    type: SUBMIT_PLAYER_FORM,
    payload: data
  };
};

export const submitUpdatePlayerForm = function(data) {
  return {
    type: SUBMIT_UPDATE_PLAYER_FORM,
    payload: data
  };
};

export const getTournamentById =  (tournamentId, requirePicture) => {
  return {
    type: GET_TOURNAMENT_BY_ID,
    tournamentId: tournamentId,
    requirePicture : requirePicture
  };
};

export const getAllTournaments =  (requirePicture) => {
  return {
    type: GET_ALL_TOURNAMENTS,
    requirePicture: requirePicture
  };
};

export const getAllLocations =  () => {
  return {
    type: GET_ALL_LOCATIONS
  };
};

export const getLocationById =  (locationId) => {
  return {
    type: GET_LOCATION_BY_ID,
    locationId: locationId
  };
};

export const getAllClubs =  () => {
  return {
    type: GET_ALL_CLUBS
  };
};

export const getClubById =  (clubId) => {
  return {
    type: GET_CLUB_BY_ID,
    clubId: clubId
  };
};

export const getAllPlayers =  () => {
  return {
    type: GET_ALL_PLAYERS
  };
};

export const getPlayerById =  (playerId) => {
  return {
    type: GET_PLAYER_BY_ID,
    playerId: playerId
  };
};

export const uploadTournamentPicture =  (tournamentId, file) => {
  return {
    type: UPDATE_TOURNAMENT_PICTURE,
    payload: {
      tournamentId,
      file
    }
  };
};

export const getTournamentPicture =  (tournamentId) => {
  return {
    type: GET_TOURNAMENT_PICTURE,
    tournamentId: tournamentId
  };
};

export const deleteLocations = (locations) => {
  return {
    type: DELETE_LOCATIONS,
    locations: locations
  }
}

export const getGameResults = (tournamentId) => {
  return {
    type: GET_GAME_RESULTS,
    tournamentId: tournamentId
  }
}

export const getAllGamesByTournamentId = (tournamentId) => {
  return{
    type: GET_ALL_GAMES_BY_TOURNAMENT_ID,
    tournamentId: tournamentId
  }
}

export const getGame = (gameId) => {
  return{
    type: GET_GAME,
    gameId: gameId
  }
}

export const getTournamentPairs =  (tournamentId) => {
    return {
        type: GET_TOURNAMENT_PAIRS,
        tournamentId: tournamentId,
    };
};

export const getAllPairs =  () => {
    return {
        type: GET_ALL_PAIRS
    };
};