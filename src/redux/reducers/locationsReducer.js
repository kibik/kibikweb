import { 
    GET_ALL_LOCATIONS_SUCCESS,
    GET_LOCATION_BY_ID_SUCCESS
} from './types'

export default (state = {}, action) => {
    switch (action.type) {
        case GET_ALL_LOCATIONS_SUCCESS:
            return { ...state, locations: action.locations };
        case GET_LOCATION_BY_ID_SUCCESS:
            return { ...state, location: action.location }
        default:
            return state;
    }
}