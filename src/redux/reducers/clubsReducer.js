import { 
    GET_ALL_CLUBS_SUCCESS,
    GET_CLUB_BY_ID_SUCCESS
} from './types'

export default (state = {}, action) => {
    switch (action.type) {
        case GET_ALL_CLUBS_SUCCESS:
            return { ...state, clubs: action.clubs };
        case GET_CLUB_BY_ID_SUCCESS:
            return { ...state, club: action.club }
        default:
            return state;
    }
}