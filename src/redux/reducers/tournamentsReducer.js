import { 
    GET_ALL_TOURNAMENTS_SUCCESS,
    GET_TOURNAMENT_BY_ID_SUCCESS,
    GET_TOURNAMENT_PICTURE_SUCCESS
} from './types'

export default (state = {}, action) => {
    switch (action.type) {
        case GET_ALL_TOURNAMENTS_SUCCESS:
            return { ...state, tournaments: action.tournaments };
        case GET_TOURNAMENT_BY_ID_SUCCESS:
            return { ...state, tournament: action.tournament };   
        case GET_TOURNAMENT_PICTURE_SUCCESS:
            debugger;
            if(!state.tournaments) {
                return { ...state, tournament: { ...state.tournament, Logo: action.tournamentPicture}  }; 
            }

            let tournament = state.tournaments.find(t => t.Id === action.tournamentId);
            tournament.Logo = action.tournamentPicture;
            return { ...state, tournaments: {...state.tournaments}};   
        default:
            return state;
    }
}