import { 
    CREATE_TOURNAMENT_SUCCESS,
    DELETE_LOCATIONS_SUCCESS,
    GET_TOURNAMENT_PAIRS_SUCCESS
} from './types'
import differenceBy from 'lodash/differenceBy';

export default (state = {}, action) => {
    switch (action.type) {
        case CREATE_TOURNAMENT_SUCCESS:
            return { ...state, tournaments: action.tournaments };
        case DELETE_LOCATIONS_SUCCESS:
            let dd = differenceBy(state.locations, action.deletedLocations, 'id')
            return { ...state, locations: dd };
        case GET_TOURNAMENT_PAIRS_SUCCESS:
            return { ...state, tournamentPairs : action.tournamentPairs}
        default:
            return state;
    }
}