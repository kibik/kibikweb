import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import gamesReducer  from './gamesReducer';
import tournamentsReducer  from './tournamentsReducer';
import manageReducer from './manageReducer';
import locationsReducer from './locationsReducer';
import clubsReducer from './clubsReducer';
import playersReducer from './playersReducer';
import pairsReducer from './pairsReducer';

export default combineReducers({
  form: formReducer,
  games: gamesReducer,
  tournaments: tournamentsReducer,
  manage: manageReducer,
  locations: locationsReducer,
  clubs: clubsReducer,
  players: playersReducer,
  pairs: pairsReducer,
});