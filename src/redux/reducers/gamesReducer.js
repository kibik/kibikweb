import { 
    FETCH_GAMES,
    FETCH_GAME,
    FETCH_GAME_DETAILS,
    FETCH_COMMENTS_BY_GAME,
} from '../actions/types'

import{
    GET_GAME_RESULTS_SUCCESS,
    GET_GAME_SUCCESS,
    GET_ALL_GAMES_BY_TOURNAMENT_ID_SUCCESS
} from '../reducers/types';

export default (state = {}, action) => {
    switch (action.type) {
        case FETCH_GAMES:
            return { ...state, games: action.payload };
        case FETCH_GAME_DETAILS:
            return { ...state, gameDetails: action.payload};
        case FETCH_GAME:
            return { ...state, game: action.payload};
        case FETCH_COMMENTS_BY_GAME:
            return { ...state, comments: action.payload};


        case GET_GAME_RESULTS_SUCCESS:
            return { ...state, results : action.results}
        case GET_GAME_SUCCESS:
            return { ...state, game : action.game}
        case GET_ALL_GAMES_BY_TOURNAMENT_ID_SUCCESS:
            return { ...state, rounds: action.rounds}
        default:
            return state;
    }
}