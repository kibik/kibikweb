import { 
    GET_ALL_PAIRS_SUCCESS,
} from './types'

export default (state = {}, action) => {
    switch (action.type) {
        case GET_ALL_PAIRS_SUCCESS:
            return { ...state, pairs: action.pairs };
        default:
            return state;
    }
}