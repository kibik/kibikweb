import { 
    GET_ALL_PLAYERS_SUCCESS,
    GET_PLAYER_BY_ID_SUCCESS
} from './types'

export default (state = {}, action) => {
    switch (action.type) {
        case GET_ALL_PLAYERS_SUCCESS:
            return { ...state, players: action.players };
        case GET_PLAYER_BY_ID_SUCCESS:
            return { ...state, player: action.player }
        default:
            return state;
    }
}